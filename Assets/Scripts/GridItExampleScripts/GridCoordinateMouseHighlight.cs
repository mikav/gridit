using UnityEngine;
using System.Collections;

namespace GridIt
{
	public class GridCoordinateMouseHighlight : MonoBehaviour
	{
		private GridRenderer gridRenderer;

		public void Start()
		{
			gridRenderer = FindObjectOfType<GridRenderer>();
			if (gridRenderer != null && GridInputManager.Instance != null)
			{
				GridInputManager.Instance.MouseCoordinateChanged += OnMouseCoordinateChanged;
			}
		}

		private void OnMouseCoordinateChanged(GridCoordinate previousCoordinate, GridCoordinate coordinate)
		{
			if (previousCoordinate.Valid)
			{
				gridRenderer.SetColor(previousCoordinate, new Color(1.0f, 1.0f, 1.0f, 1.0f));
			}
			if (coordinate.Valid)
			{
				gridRenderer.SetColor(coordinate, new Color(1.0f, 0.0f, 0.0f, 1.0f));
			}
		}
	}
}