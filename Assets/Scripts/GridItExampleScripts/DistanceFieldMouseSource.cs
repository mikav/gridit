using UnityEngine;
using System.Collections;
using GridIt;

public class DistanceFieldMouseSource : GridSource
{
	public void Start()
	{
		if (GridInputManager.Instance != null)
		{
			GridInputManager.Instance.MouseCoordinateChanged += OnMouseCoordinateChanged;
		}
	}

	private void OnMouseCoordinateChanged(GridCoordinate previousCoordinate, GridCoordinate coordinate)
	{
		if (coordinate.Valid)
		{
			transform.position = Grid.Instance.GetPosition(coordinate);
		}
	}
}
