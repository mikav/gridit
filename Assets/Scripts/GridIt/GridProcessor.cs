using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridIt
{
	public delegate void ProcessorDeactivatedHandler(GridProcessor processor);

	public abstract class GridProcessor : MonoBehaviour
	{
		/// <summary>
		/// This event handler is invoked when processor is about to be destroyed or otherwise premanently deactivated.
		/// </summary>
		public event ProcessorDeactivatedHandler ProcessorDeactivated;

		[SerializeField, Tooltip("Is debug drawing enabled for this processor. If multiple processors have this field set to true then the first in the list owned by the Grid will be used.")]
		private bool debugDraw;

		void OnDestroy()
		{
			UninitProcessor();

			ProcessorDeactivatedHandler handler = ProcessorDeactivated;
			if (handler != null)
			{
				handler(this);
			}
		}

		/// <summary>
		/// Helper to easily access the current Grid instance
		/// </summary>
		protected Grid Grid
		{
			get
			{
				return Grid.Instance;
			}
		}

		#region Abstract methods

		/// <summary>
		/// Update processor one more iteration, if true is returned all processing is done for this iteration.
		/// </summary>
		public abstract bool UpdateIteration();

		/// <summary>
		/// This is called before UpdateProcessor() if update iteration has not started yet. It is paired with UpdateIterationEnd() which is called after UpdateProcessor() returns true.
		/// </summary>
		public virtual void UpdateIterationStart()
		{
		}

		/// <summary>
		/// This is called after UpdateProcessor() returns true, this call is done right after the call to UpdateProcessor by GridProcessorUpdater.
		/// </summary>
		public virtual void UpdateIterationEnd()
		{
		}

		/// <summary>
		/// Is update iteration in progress.
		/// </summary>
		public abstract bool UpdateIterationInProgress
		{
			get;
		}

		/// <summary>
		/// Initialize processor.
		/// </summary>
		public abstract void InitProcessor();

		/// <summary>
		/// Uninitialize processor.
		/// </summary>
		public virtual void UninitProcessor()
		{
		}

		/// <summary>
		/// This will call UpdateIterationStart(), UpdateIteration() until it returns true and then UpdateIterationEnd().
		/// </summary>
		public void UpdateFullIterationCycle(int maxNumberOfIterations = 1000)
		{
			UpdateIterationStart();
			int count = 0;
			while (!UpdateIteration() && count < maxNumberOfIterations)
			{
				count++;
			}
			if (count == maxNumberOfIterations)
			{
				// We got here because UpdateIteration() did not return true even after 'maxNumberOfIterations' consequent calls to it. Potential infinite loop.
				Debug.LogError("UpdateFullIterationCycle() for " + this.ToString() + " did not complete UpdateIteration() even after " + maxNumberOfIterations + " consequent calls, potential infinite loop!");
			}
			UpdateIterationEnd();
		}

		#endregion

		/// <summary>
		/// By default all processors request for continuous update.
		/// </summary>
		public virtual bool NeedsUpdate
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Add all grid sources matching the layer mask into the given list.
		/// </summary>
		protected void AddGridSources(List<GridSource> sourceList, int layerMask)
		{
			List<GridSource> gridSources = Grid.Instance.GridSources;
			foreach (GridSource source in gridSources)
			{
				if (source.HasCompatibleLayer(layerMask))
				{
					sourceList.Add(source);
				}
			}
		}

		#region Debug drawing

#if UNITY_EDITOR

		/// <summary>
		/// Is debug drawing enabled
		/// </summary>
		public bool DebugDraw
		{
			get
			{
				return debugDraw;
			}
		}

		/// <summary>
		/// Draw single cell
		/// </summary>
		public virtual void DebugDrawCell(int offset)
		{
			Gizmos.color = GetCellColor(offset);
			GridCoordinate coordinate = new GridCoordinate(offset);
			Vector3 position = coordinate.ToVector3();
			Gizmos.DrawCube(position, Grid.Instance.CellSize2d);
		}

		private static Color WhiteCell = new Color(1.0f, 1.0f, 1.0f, 0.7f);

		/// <summary>
		/// Get cell color for specified offset
		/// </summary>
		public virtual Color GetCellColor(int offset)
		{
			return WhiteCell;
		}

#endif

		#endregion
	}
}
