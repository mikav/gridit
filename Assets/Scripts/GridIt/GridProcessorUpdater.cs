﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace GridIt
{
	/// <summary>
	/// Helper class to setup update pipelines for the grid processor updater
	/// </summary>
	[Serializable]
	public class UpdatePipeline
	{
		[Tooltip("Pipeline id, used if processors are added from script when adding them to the Updater.")]
		public int id = 0;
		[Tooltip("How many microseconds can be used per one update on this pipeline, 0 will disable the limitation. 1 second = 1000 milliseconds, 1 millisecond = 1000 microseconds. Game running 30FPS can use maximum of 33.3 ms / frame which is about 33333 microseconds.")]
		public int microsecondsPerUpdate = 0;
		[Tooltip("Grid processors in this pipeline.")]
		public List<GridProcessor> gridProcessors = new List<GridProcessor>();

		[HideInInspector]
		public int currentProcessorUpdateIndex;
	}

	/// <summary>
	/// This class / component is used to update grid processors setup for different pipelines.
	/// Each pipeline can have any number of processors which are updated sequentially. Each pipeline has also the number of microseconds they can spend per frame to update the processor(s).
	/// Since each processor can specify the number of iterations per call to their UpdateIteration() they can consume more microseconds than allowed for the pipeline.
	/// If all processors on one pipeline can be updated fully within the given budget then the update will end anyway since there is no point to update processors more than once per frame.
	/// There is also a setting on the entire updater to control the max number of microseconds to use for any of the pipelines which will clamp individual pipeline setting.
	/// This class will consume a lot of processing power depending on how many and what kind of grid processors it updates. Also, it depends on the pipeline settings for allowed update time.
	/// </summary>
	public class GridProcessorUpdater : MonoBehaviour
	{
		[SerializeField, Tooltip("Update pipelines for this GridProcessorUpdater.")]
		private List<UpdatePipeline> updatePipelines = new List<UpdatePipeline>();

		[SerializeField, Tooltip("Maximum microseconds per update to spend on one single processor.")]
		private int maxMicrosecondsPerPipeline = 1000000;

		/// <summary>
		/// Get update pipeline info for given pipeline id or null if not found.
		/// </summary>
		private UpdatePipeline GetUpdatePipeline(int pipelineId)
		{
			foreach (UpdatePipeline pipeline in updatePipelines)
			{
				if (pipeline.id == pipelineId)
				{
					return pipeline;
				}
			}
			return null;
		}

		/// <summary>
		/// Register processor to a specific (existing) pipeline.
		/// </summary>
		public void RegisterProcessor(GridProcessor processor, int pipelineId)
		{
			UpdatePipeline pipeline = GetUpdatePipeline(pipelineId);
			if (pipeline != null)
			{
				pipeline.gridProcessors.Add(processor);
				processor.InitProcessor();
			}
			else
			{
				Debug.LogError("Tried to register processor for invalid pipeline id (" + pipelineId + "). Make sure to add pipelines for updater!");	
			}
		}

		/// <summary>
		/// Unregister processor from which ever pipeline it might be in.
		/// </summary>
		public void UnregisterProcessor(GridProcessor processor)
		{
			foreach (UpdatePipeline pipeline in updatePipelines)
			{
				int index = -1;
				for (int i = 0; i < pipeline.gridProcessors.Count; i++)
				{
					if (pipeline.gridProcessors[i] == processor)
					{
						index = i;
						break;
					}
				}
				if (index >= 0)
				{
					pipeline.gridProcessors.RemoveAt(index);
					if (index < pipeline.currentProcessorUpdateIndex)
					{
						// Reduce the currentProcessorUpdateIndex so that we won't switch to update another processor due to deletion of one
						pipeline.currentProcessorUpdateIndex--;
					}
				}
			}
		}

		/// <summary>
		/// Update single pipeline
		/// </summary>
		private void UpdatePipeline(UpdatePipeline pipeline)
		{
			// Figure out how many microseconds to spend during this update for this pipeline, maximum is 1 second.
			long ticksPerMicrosecond = (TimeSpan.TicksPerMillisecond / 1000);
			long maxTicks = (pipeline.microsecondsPerUpdate > 0 && pipeline.microsecondsPerUpdate <= maxMicrosecondsPerPipeline ? pipeline.microsecondsPerUpdate : maxMicrosecondsPerPipeline) * ticksPerMicrosecond;
			long startTime = DateTime.Now.Ticks;
			long deltaTicks = 0;

			// Iterate each processor once at maximum
			for (int i = 0; i < pipeline.gridProcessors.Count; i++)
			{
				// Make sure currentProcessorUpdateIndex is within bounds (processors may be removed / added between calls to UpdatePipeline)
				pipeline.currentProcessorUpdateIndex = Mathf.Max(0, pipeline.currentProcessorUpdateIndex % pipeline.gridProcessors.Count);

				// Get current processor to update
				GridProcessor currentProcessor = pipeline.gridProcessors[pipeline.currentProcessorUpdateIndex];
				if (currentProcessor.NeedsUpdate)
				{
					// Processor needs update
					if (!currentProcessor.UpdateIterationInProgress)
					{
						// New iteration cycle starts -> Call IterationStart
						currentProcessor.UpdateIterationStart();
					}

					// Call UpdateIteration() until out of processing time or the update iteration cycle is completed for this processor.
					while (true)
					{
						deltaTicks = DateTime.Now.Ticks - startTime;
						if (deltaTicks >= maxTicks)
						{
							// No time left to update
							return;
						}

						if (currentProcessor.UpdateIteration())
						{
							// Update iteration cycle done -> Break
							break;
						}
					}

					// Iteration cycle ends -> Call IterationEnd
					currentProcessor.UpdateIterationEnd();
				}

				// Update currentProcessorUpdateIndex as we have completed the current processor update cycle
				pipeline.currentProcessorUpdateIndex++;

				deltaTicks = DateTime.Now.Ticks - startTime;
				if (deltaTicks > maxTicks)
				{
					// No time left to update
					return;
				}
			}
		}

		/// <summary>
		/// Unity engine start() for the component
		/// </summary>
		private void Start()
		{
			// Call InitProcessor for all design-time added processors
			foreach (UpdatePipeline pipeline in updatePipelines)
			{
				foreach (GridProcessor processor in pipeline.gridProcessors)
				{
					processor.InitProcessor();
				}
			}
		}

		/// <summary>
		/// Update after all gameObject regular updates have been called to have the latest data available.
		/// </summary>
		private void LateUpdate()
		{
			// Update all pipelines every frame -> This may not actually perform any calculation if there is nothing to update
			foreach (UpdatePipeline pipeline in updatePipelines)
			{
				UpdatePipeline(pipeline);
			}
		}
	}
}

