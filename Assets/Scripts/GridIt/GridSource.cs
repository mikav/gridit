﻿using UnityEngine;
using System.Collections;
using System;

namespace GridIt
{
	/// <summary>
	/// Component for tagging gameObject as grid source for various grid processors. Distance field operates using these as the source points.
	/// Each source has a layer (between 0 and 31) and processors have corresponding layer mask to access grid sources belonging to specific layer(s).
	/// Layer is immutable and can be set only from the property editor in the Unity editor.
	/// </summary>
	public class GridSource : MonoBehaviour
	{
		/// <summary>
		/// Layer for this grid source
		/// </summary>
		public int Layer
		{
			get
			{
				return layer;
			}
		}

		/// <summary>
		/// Is the layer compatible with given mask.
		/// </summary>
		public bool HasCompatibleLayer(int layerMask)
		{
			return ((1 << layer) & (uint)layerMask) != 0;
		}

		/// <summary>
		/// This method is called from Unity OnEnable() method. Override this to perform any initialization needed, remember to call base.Init().
		/// </summary>
		public virtual void Init()
		{
			if (Grid.Instance != null)
			{
				Grid.Instance.RegisterGridSource(this);
			}
		}

		/// <summary>
		/// This method is called from Unity OnDisable() method. Override this to perform any uninitialization needed, remember to call base.Uninit().
		/// </summary>
		public virtual void Uninit()
		{
			if (Grid.Instance != null)
			{
				Grid.Instance.UnregisterGridSource(this);
			}
		}

		/// <summary>
		/// Initialize grid source.
		/// </summary>
		private void OnEnable()
		{
			Init();
		}

		/// <summary>
		/// Upon destruction of this component call Uninit().
		/// </summary>
		private void OnDisable()
		{
			Uninit();
		}

		[SerializeField, Range(0, 31), Tooltip("Source layer, number between 0 and 31. Layer mask is created with (1 << layer) and is used in some of the processors.")]
		private int layer;
	}
}
