using UnityEngine;
using System.Collections;

namespace GridIt
{
	public delegate void MouseCoordinateChangeHandler(GridCoordinate previousCoordinate, GridCoordinate currentCoordinate);

	/// <summary>
	/// Utility class to track mouse over the Grid.
	/// </summary>
	public class GridInputManager : MonoBehaviour
	{
		public event MouseCoordinateChangeHandler MouseCoordinateChanged;

		private GridCoordinate previousMouseGridCoordinate = GridCoordinate.Invalid;
		private GridCoordinate mouseGridCoordinate = GridCoordinate.Invalid;

		private static GridInputManager instance;

		/// <summary>
		/// Return grid input manager singleton instance
		/// </summary>
		public static GridInputManager Instance
		{
			get
			{
#if UNITY_EDITOR
				if (!Application.isPlaying)
				{
					return FindObjectOfType<GridInputManager>();
				}
#endif
				return instance;
			}
			private set
			{
				instance = value;
			}
		}
		
		/// <summary>
		/// Initialize instance
		/// </summary>
		void OnEnable()
		{
			if (Instance != null)
			{
				Debug.LogError("Critical error! More than one active GridInputManager instance, this is not supported and will lead into problems!");
			}
			Instance = this;
		}

		/// <summary>
		/// When Unity disables this component we set the Instance to null so no-one can access us through it.
		/// </summary>
		void OnDisble()
		{
			Instance = null;
		}

		private GridCoordinate MouseGridCoordinate
		{
			get
			{
				return mouseGridCoordinate;
			}
			set
			{
				if (mouseGridCoordinate != value)
				{
					previousMouseGridCoordinate = mouseGridCoordinate;
					mouseGridCoordinate = value;

					NotifyMouseCoordinateChanged(previousMouseGridCoordinate, mouseGridCoordinate);
				}
			}
		}

		private void NotifyMouseCoordinateChanged(GridCoordinate previous, GridCoordinate current)
		{
			MouseCoordinateChangeHandler handler = MouseCoordinateChanged;
			if (handler != null)
			{
				handler(previous, current);
			}
		}

		private void Update()
		{
			Grid grid = Grid.Instance;
			if (grid != null)
			{
				RaycastHit hitInfo;
			
				if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
				{
					Vector3 collisionPoint = hitInfo.point;
					MouseGridCoordinate = grid.GetGridCoordinate(collisionPoint);
				}
				else
				{
					MouseGridCoordinate = GridCoordinate.Invalid;
				}
			}
		}
	}
}