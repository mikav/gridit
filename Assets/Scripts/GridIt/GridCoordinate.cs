using UnityEngine;

namespace GridIt
{
	/// <summary>
	/// Grid coordinate struct
	/// </summary>
	public struct GridCoordinate
	{
		public static GridCoordinate Invalid = new GridCoordinate(-1, -1);

		public GridCoordinate(int inX, int inY)
		{
			x = inX;
			y = inY;
		}

		public GridCoordinate(int inOffset)
		{
			x = inOffset % Grid.Instance.Width;
			y = inOffset / Grid.Instance.Width;
		}

		public bool Valid
		{
			get
			{
				return x >= 0 && y >= 0 && x < Grid.Instance.Width && y < Grid.Instance.Height;
			}
		}

		public int Offset
		{
			get
			{
				return y * Grid.Instance.Width + x;
			}
		}

		public override bool Equals(object o)
		{
			if (o == null || o.GetType() != typeof(GridCoordinate))
			{
				return false;
			}

			GridCoordinate c = (GridCoordinate)o;
			return (x == c.x) && (y == c.y);
		}

		public override int GetHashCode()
		{
			return x ^ y;
		}

		public static bool operator ==(GridCoordinate a, GridCoordinate b)
		{
			return a.x == b.x && a.y == b.y;
		}

		public static bool operator !=(GridCoordinate a, GridCoordinate b)
		{
			return a.x != b.x || a.y != b.y;
		}

		public GridCoordinate GetNeighbor(int neighborIndex)
		{
			return neighborIndex >= 0 && neighborIndex <= 8 ? new GridCoordinate(x + xNeighborDir[neighborIndex], y + yNeighborDir[neighborIndex]) : GridCoordinate.Invalid;
		}

		public void Clamp()
		{
			x = Mathf.Clamp(x, 0, Grid.Instance.Width - 1);
			y = Mathf.Clamp(y, 0, Grid.Instance.Height - 1);
		}

		public override string ToString()
		{
			return "X: " + x + " Y: " + y;
		}

		public int x;
		public int y;

		public readonly static int[] xNeighborDir = new int[] { 0, 1, 1, 1, 0, -1, -1, -1 };
		public readonly static int[] yNeighborDir = new int[] { -1, -1, 0, 1, 1, 1, 0, -1 };
	}
}