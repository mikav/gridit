#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System;

namespace GridIt
{
	public enum CellHeightRenderingMode
	{
		IgnoreCellHeight,
		NonContinuousSurface,
		ContinuousSurface
	}

	public class MeshData
	{
		public void AddQuad(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Vector2 uvA, Vector2 uvB, Vector2 uvC, Vector2 uvD, Color color, Vector3 normal)
		{
			vertices[vertexOffset + 0] = a;
			vertices[vertexOffset + 1] = b;
			vertices[vertexOffset + 2] = c;
			vertices[vertexOffset + 3] = d;

			colors[vertexOffset + 0] = color;
			colors[vertexOffset + 1] = color;
			colors[vertexOffset + 2] = color;
			colors[vertexOffset + 3] = color;

			normals[vertexOffset + 0] = normal;
			normals[vertexOffset + 1] = normal;
			normals[vertexOffset + 2] = normal;
			normals[vertexOffset + 3] = normal;

			uv[vertexOffset + 0] = uvA;
			uv[vertexOffset + 1] = uvB;
			uv[vertexOffset + 2] = uvC;
			uv[vertexOffset + 3] = uvD;

			triangles[triangleOffset + 0] = vertexOffset + 0;
			triangles[triangleOffset + 1] = vertexOffset + 2;
			triangles[triangleOffset + 2] = vertexOffset + 1;

			triangles[triangleOffset + 3] = vertexOffset + 0;
			triangles[triangleOffset + 4] = vertexOffset + 3;
			triangles[triangleOffset + 5] = vertexOffset + 2;

			vertexOffset += 4;
			triangleOffset += 6;
		}

		public void AddQuad(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Vector2 uvA, Vector2 uvB, Vector2 uvC, Vector2 uvD, Color color, Vector3 normal, int x, int y, int width, int height, float baseY, float[] heightField)
		{
			int left = Mathf.Max(x - 1 , 0);
			int right = Mathf.Min(x + 1, width - 1);
			int up = Mathf.Max(y - 1, 0);
			int down = Mathf.Min(y + 1, height - 1);

			float ulHeight = ((a.y + baseY) + heightField[up * width + left] + heightField[up * width + x] + heightField[y * width + left]) * 0.25f - baseY;
			float urHeight = ((a.y + baseY) + heightField[up * width + right] + heightField[up * width + x] + heightField[y * width + right]) * 0.25f - baseY;
			float llHeight = ((a.y + baseY) + heightField[down * width + left] + heightField[down * width + x] + heightField[y * width + left]) * 0.25f - baseY;
			float lrHeight = ((a.y + baseY) + heightField[down * width + right] + heightField[down * width + x] + heightField[y * width + right]) * 0.25f - baseY;

			vertices[vertexOffset + 0] = a +				new Vector3(0.0f, ulHeight - a.y, 0.0f);
			vertices[vertexOffset + 1] = (a + b) * 0.5f +	new Vector3(0.0f, (a.y + (heightField[up * width + x] -			baseY)) * 0.5f - a.y, 0.0f);
			vertices[vertexOffset + 2] = b +				new Vector3(0.0f, urHeight - a.y, 0.0f);

			vertices[vertexOffset + 3] = (a + d) * 0.5f +	new Vector3(0.0f, (a.y + (heightField[y * width + left] -		baseY)) * 0.5f - a.y, 0.0f);
			vertices[vertexOffset + 4] = (a + b + c + d) * 0.25f;
			vertices[vertexOffset + 5] = (b + c) * 0.5f +	new Vector3(0.0f, (a.y + (heightField[y * width + right] -		baseY)) * 0.5f - a.y, 0.0f);

			vertices[vertexOffset + 6] = d +				new Vector3(0.0f, llHeight - a.y, 0.0f);
			vertices[vertexOffset + 7] = (c + d) * 0.5f +	new Vector3(0.0f, (a.y + (heightField[down * width + x] -		baseY)) * 0.5f - a.y, 0.0f);
			vertices[vertexOffset + 8] = c +				new Vector3(0.0f, lrHeight - a.y, 0.0f);

			for (int i = 0; i < 9; i++)
			{
				colors[vertexOffset + i] = color;
				normals[vertexOffset + i] = normal;
			}

			uv[vertexOffset + 0] = uvA;
			uv[vertexOffset + 1] = (uvA + uvB) * 0.5f;
			uv[vertexOffset + 2] = uvB;

			uv[vertexOffset + 3] = (uvA + uvD) * 0.5f;
			uv[vertexOffset + 4] = (uvA + uvB + uvC + uvD) * 0.25f;
			uv[vertexOffset + 5] = (uvB + uvC) * 0.5f;

			uv[vertexOffset + 6] = uvD;
			uv[vertexOffset + 7] = (uvC + uvD) * 0.5f;
			uv[vertexOffset + 8] = uvC;

			triangles[triangleOffset + 0] = vertexOffset + 0;
			triangles[triangleOffset + 1] = vertexOffset + 4;
			triangles[triangleOffset + 2] = vertexOffset + 1;
			triangles[triangleOffset + 3] = vertexOffset + 0;
			triangles[triangleOffset + 4] = vertexOffset + 3;
			triangles[triangleOffset + 5] = vertexOffset + 4;

			triangles[triangleOffset + 6] = vertexOffset + 1;
			triangles[triangleOffset + 7] = vertexOffset + 5;
			triangles[triangleOffset + 8] = vertexOffset + 2;
			triangles[triangleOffset + 9] = vertexOffset + 1;
			triangles[triangleOffset + 10] = vertexOffset + 4;
			triangles[triangleOffset + 11] = vertexOffset + 5;

			triangles[triangleOffset + 12] = vertexOffset + 3;
			triangles[triangleOffset + 13] = vertexOffset + 7;
			triangles[triangleOffset + 14] = vertexOffset + 4;
			triangles[triangleOffset + 15] = vertexOffset + 3;
			triangles[triangleOffset + 16] = vertexOffset + 6;
			triangles[triangleOffset + 17] = vertexOffset + 7;

			triangles[triangleOffset + 18] = vertexOffset + 4;
			triangles[triangleOffset + 19] = vertexOffset + 8;
			triangles[triangleOffset + 20] = vertexOffset + 5;
			triangles[triangleOffset + 21] = vertexOffset + 4;
			triangles[triangleOffset + 22] = vertexOffset + 7;
			triangles[triangleOffset + 23] = vertexOffset + 8;

			vertexOffset += 9;
			triangleOffset += 24;
		}

		public int vertexOffset;
		public int triangleOffset;

		public Vector3[] vertices;
		public Color[] colors;
		public Vector3[] normals;
		public Vector2[] uv;
		public int[] triangles;
	}

	public class GridRenderer : MonoBehaviour
	{
		[Tooltip("Offset along Y-axis for grid mesh.")]
		public float meshYOffset = 0.05f;

		[Tooltip("How to render cell heights, this data exists only while application is running and has no effect while in editor edit mode.")]
		public CellHeightRenderingMode HeightRenderMode = CellHeightRenderingMode.IgnoreCellHeight;

		private GridField<float> heightField;

		/// <summary>
		/// Set color of a grid coordinate
		/// </summary>
		public void SetColor(GridCoordinate coordinate, Color color)
		{
			if (coordinate.Valid)
			{
				int verticesPerCell = (HeightRenderMode == CellHeightRenderingMode.IgnoreCellHeight || HeightRenderMode == CellHeightRenderingMode.NonContinuousSurface) ? 4 : 9;

				int offset = coordinate.Offset * verticesPerCell;
				MeshFilter meshFilter = GetComponent<MeshFilter>();
				if (meshFilter != null)
				{
					Color[] colors = meshFilter.sharedMesh.colors;
					if (colors != null && offset + verticesPerCell <= colors.Length)
					{
						for (int j = 0; j < verticesPerCell; j++)
						{
							colors[offset + j] = color;
						}
						meshFilter.sharedMesh.colors = colors;
					}
				}
			}
		}

		/// <summary>
		/// Set colors for grid cells (has to have grid.width * grid.height elements in the given array). 
		/// Use SetColor(GridCoordinate, Color) for setting invidivual cell color.
		/// </summary>
		public void SetColors(Color[] cellColors)
		{
			MeshFilter meshFilter = GetComponent<MeshFilter>();
			int verticesPerCell = (HeightRenderMode == CellHeightRenderingMode.IgnoreCellHeight || HeightRenderMode == CellHeightRenderingMode.NonContinuousSurface) ? 4 : 9;

			if (meshFilter != null && cellColors != null && cellColors.Length == meshFilter.sharedMesh.colors.Length / verticesPerCell)
			{
				Color[] colors = meshFilter.sharedMesh.colors;

				for (int i = 0; i < cellColors.Length; i++)
				{
					for (int j = 0; j < verticesPerCell; j++)
					{
						colors[i * verticesPerCell + j] = cellColors[i];
					}
				}

				meshFilter.sharedMesh.colors = colors;
			}
		}

		/// <summary>
		/// Return Grid component from shared gameobject
		/// </summary>
		private Grid Grid
		{
			get
			{
				// Opt for grid component from the same game object
				Grid grid = GetComponent<Grid>();
				if (grid == null)
				{
					// Try to find global active grid instance
					grid = Grid.Instance;
				}
				return grid;
			}
		}

#if UNITY_EDITOR
		private void OnValidate()
		{
			CreateMesh();

			if (!Application.isPlaying && Grid != null)
			{
				Grid.GridPropertiesChanged -= OnGridPropertiesChanged;
				Grid.GridPropertiesChanged += OnGridPropertiesChanged;
			}
		}
#endif

		/// <summary>
		/// When component is enabled create mesh, if in editor register for grid property changes
		/// </summary>
		private void OnEnable()
		{
			CreateMesh();

			heightField = Grid.Instance.CellWorldHeights;
			if (heightField != null)
			{
				heightField.Flipped -= OnHeightFieldFlipped;
				heightField.Flipped += OnHeightFieldFlipped;
			}
		}

		/// <summary>
		/// Called when (potential) height field is flipped. This means it has been updated and we need to update mesh if height affects the mesh.
		/// </summary>
		private void OnHeightFieldFlipped()
		{
			if (HeightRenderMode != CellHeightRenderingMode.IgnoreCellHeight)
			{
				UpdateMesh();
			}
		}

		/// <summary>
		/// When disabled destroy the mesh, if in editor unregister from grid property changes
		/// </summary>
		private void OnDisable()
		{
			DestroyMesh();

			GridField<float> heightField = Grid.Instance.CellWorldHeights;
			if (heightField != null)
			{
				heightField.Flipped -= OnHeightFieldFlipped;
			}
		}
		
		/// <summary>
		/// Try to get heightfield as it is created lazily
		/// </summary>
		public void Update()
		{
			if (HeightRenderMode != CellHeightRenderingMode.IgnoreCellHeight && heightField == null && Grid.Instance.CellWorldHeights != null)
			{
				heightField = Grid.Instance.CellWorldHeights;
				heightField.Flipped -= OnHeightFieldFlipped;
				heightField.Flipped += OnHeightFieldFlipped;

				OnHeightFieldFlipped();
			}
		}
		
		/// <summary>
		/// Create mesh and meshfilter if it does not exist already
		/// </summary>
		private void CreateMesh()
		{
			if (GetComponent<MeshFilter>() == null)
			{
				gameObject.AddComponent<MeshFilter>();
#if UNITY_EDITOR
				if (!Application.isPlaying)
				{
					// If in editor (and not in play mode) set gameobject dirty
					EditorUtility.SetDirty(gameObject);
				}
#endif
			}

			UpdateMesh();
		}

		/// <summary>
		/// Destroy mesh
		/// </summary>
		private void DestroyMesh()
		{
			MeshFilter meshFilter = GetComponent<MeshFilter>();
			if (meshFilter != null)
			{
				meshFilter.sharedMesh = null;
			}
		}


		/// <summary>
		/// Update mesh to reflect grid properties
		/// </summary>
		private void UpdateMesh()
		{
			Grid grid = Grid;
			MeshFilter meshFilter = GetComponent<MeshFilter>();
			if (grid != null && meshFilter != null)
			{
				CellHeightRenderingMode heightRenderMode = heightField == null ? CellHeightRenderingMode.IgnoreCellHeight : HeightRenderMode;

				int verticesPerCell = (heightRenderMode == CellHeightRenderingMode.IgnoreCellHeight || heightRenderMode == CellHeightRenderingMode.NonContinuousSurface) ? 4 : 9;
				int trianglesPerCell = (heightRenderMode == CellHeightRenderingMode.IgnoreCellHeight || heightRenderMode == CellHeightRenderingMode.NonContinuousSurface) ? 2 : 8;

				Mesh gridMesh = meshFilter.sharedMesh != null ? meshFilter.sharedMesh : new Mesh();

				// 4 / 9 vertices for each cell, not sharing vertices so we can color each grid cell separately
				int numVertices = grid.Width * grid.Height * verticesPerCell;
				// 2 / 8 triangles for each cell (6 / 24 indices / cell)
				int numTriangles = grid.Width * grid.Height * trianglesPerCell * 3;

				// Get / allocate buffers
				MeshData meshData = new MeshData();
				meshData.vertices = gridMesh.vertices != null && gridMesh.vertices.Length == numVertices ? gridMesh.vertices : new Vector3[numVertices];
				meshData.colors = gridMesh.colors != null && gridMesh.colors.Length == numVertices ? gridMesh.colors : new Color[numVertices];
				meshData.normals = gridMesh.normals != null && gridMesh.normals.Length == numVertices ? gridMesh.normals : new Vector3[numVertices];
				meshData.uv = gridMesh.uv != null && gridMesh.uv.Length == numVertices ? gridMesh.uv : new Vector2[numVertices];
				meshData.triangles = gridMesh.triangles != null && gridMesh.triangles.Length == numTriangles ? gridMesh.triangles : new int[numTriangles];

				meshData.vertexOffset = 0;
				meshData.triangleOffset = 0;

				Color white = new Color(1.0f, 1.0f, 1.0f, 1.0f);
				Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);
				int width = grid.Width;
				int height = grid.Height;

				// Create geometry
				for (int j = 0; j < height; j++)
				{
					for (int i = 0; i < width; i++)
					{
						float localHeight = meshYOffset;
						float baseY = grid.transform.position.y - meshYOffset;
						if (heightRenderMode != CellHeightRenderingMode.IgnoreCellHeight)
						{
							localHeight = heightField.DataRead[j * grid.Width + i] - baseY;
						}
						Vector3 position = new Vector3(i * grid.CellSize, localHeight, j * grid.CellSize);

						if (heightRenderMode == CellHeightRenderingMode.IgnoreCellHeight || heightRenderMode == CellHeightRenderingMode.NonContinuousSurface)
						{
							meshData.AddQuad(	position,
												position + new Vector3(grid.CellSize, 0.0f, 0.0f),
												position + new Vector3(grid.CellSize, 0.0f, grid.CellSize),
												position + new Vector3(0.0f, 0.0f, grid.CellSize),
												new Vector2(0.0f, 0.0f),
												new Vector2(1.0f, 0.0f),
												new Vector2(1.0f, 1.0f),
												new Vector2(0.0f, 1.0f),
												white,
												up);
						}
						else
						{
							meshData.AddQuad(	position,
												position + new Vector3(grid.CellSize, 0.0f, 0.0f),
												position + new Vector3(grid.CellSize, 0.0f, grid.CellSize),
												position + new Vector3(0.0f, 0.0f, grid.CellSize),
												new Vector2(0.0f, 0.0f),
												new Vector2(1.0f, 0.0f),
												new Vector2(1.0f, 1.0f),
												new Vector2(0.0f, 1.0f),
												white,
												up,
												i, j, width, height, baseY,
												heightField.DataRead);
						}
					}
				}

				// Set buffers to mesh (this triggers mesh update)
				gridMesh.Clear();
				gridMesh.vertices = meshData.vertices;
				gridMesh.normals = meshData.normals;
				gridMesh.colors = meshData.colors;
				gridMesh.uv = meshData.uv;
				gridMesh.triangles = meshData.triangles;
				gridMesh.name = "Grid_" + grid.Width + "x" + grid.Height;

				// Recalculate bounds and normals
				gridMesh.RecalculateBounds();
				gridMesh.RecalculateNormals();

				// Assign shared mesh
				meshFilter.sharedMesh = gridMesh;
			}
		}

#if UNITY_EDITOR
		private void OnGridPropertiesChanged()
		{
			// Update mesh to reflect current grid properties
			UpdateMesh();
		}
#endif
	}
}
