using UnityEngine;
using System.Collections;
using System;

namespace GridIt
{
	public delegate void FlippedHandler();

	/// <summary>
	/// Utility class for grid data processed and produced by GridProcessors.
	/// </summary>
	public class GridField<T>
	{
		/// <summary>
		/// Using this event handler you can listen for GridField Flip events. When someone calls Flip (usually processors) on a GridField this event handler is invoked.
		/// </summary>
		public event FlippedHandler Flipped;

		/// <summary>
		/// Constructs new double buffered GridField of elements of type T. Must specify width and height.
		/// </summary>
		public GridField(int width, int height, T defaultValue) : this(width, height, defaultValue, true)
		{
		}

		/// <summary>
		/// Constructs new GridField of elements of type T. Must specify number of elements and whether double buffering should be used.
		/// </summary>
		public GridField(int width, int height, T defaultValue, bool doubleBuffered)
		{
			int numElements = width * height;
			DataWrite = new T[numElements];
			DataRead = doubleBuffered ? new T[numElements] : DataWrite;

			DefaultValue = defaultValue;

			Clear();
		}

		/// <summary>
		/// Invoke Flipped event handler
		/// </summary>
		private void NotifyFlipped()
		{
			FlippedHandler handler = Flipped;
			if (handler != null)
			{
				handler();
			}
		}

		/// <summary>
		/// Flip buffers and clear DataWrite. If not double buffered then simply clears DataWrite.
		/// </summary>
		public void Flip(bool clearWriteBuffer = true)
		{
			if (DoubleBuffered)
			{
				T[] temp = DataRead;
				DataRead = DataWrite;
				DataWrite = temp;
			}

			if (clearWriteBuffer)
			{
				Fill(DataWrite, DefaultValue);
			}

			NotifyFlipped();
		}

		/// <summary>
		/// Value used to clear the buffer(s)
		/// </summary>
		public T DefaultValue
		{
			get;
			set;
		}

		/// <summary>
		/// Length of the buffer in elements
		/// </summary>
		public int Length
		{
			get
			{
				return DataWrite != null ? DataWrite.Length : 0;
			}
		}

		/// <summary>
		/// Buffer to read from (if not double-buffered this is the same as DataWrite)
		/// </summary>
		public T[] DataRead
		{
			get;
			private set;
		}

		/// <summary>
		/// Buffer to write into (possibly write on-going)
		/// </summary>
		public T[] DataWrite
		{
			get;
			private set;
		}

		/// <summary>
		/// Is this GridField double buffered
		/// </summary>
		public bool DoubleBuffered
		{
			get
			{
				return DataRead != DataWrite;
			}
		}

		/// <summary>
		/// Clear all buffers
		/// </summary>
		public void Clear()
		{
			Fill(DataWrite, DefaultValue);
			if (DoubleBuffered)
			{
				Fill(DataRead, DefaultValue);
			}
		}

		/// <summary>
		/// "Fast" fill of array with given value.
		/// </summary>
		private static void Fill<T2>(T2[] array, T2 value)
		{
			if (array.Length != 0)
			{
				// Set value that will be repeated
				array[0] = value;

				// Copy-repeat with doubling size (1, 2, 4, 8 etc)
				int currentCopyCount;
				for (currentCopyCount = 1; currentCopyCount <= array.Length / 2; currentCopyCount *= 2)
				{
					Array.Copy(array, 0, array, currentCopyCount, currentCopyCount);
				}

				// Fill in rest
				Array.Copy(array, 0, array, currentCopyCount, array.Length - currentCopyCount);
			}
		}
	}
}