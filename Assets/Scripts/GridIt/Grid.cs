using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

namespace GridIt
{
	/// <summary>
	/// Helper for 2x2 sample with weights
	/// </summary>
	public struct GridCoordinate2x2
	{
		public int offset00;
		public int offset10;
		public int offset01;
		public int offset11;

		public float weight00;
		public float weight10;
		public float weight01;
		public float weight11;
	}

	/// <summary>
	/// Extension methods to convert between grid coordinates and world-space positions
	/// </summary>
	public static class GridExtensionMethods
	{
		public static GridCoordinate ToGridCoordinate(this Vector3 position)
		{
			if (Grid.Instance != null)
			{
				return Grid.Instance.GetGridCoordinate(position);
			}
			return GridCoordinate.Invalid;
		}

		public static Vector3 ToVector3(this GridCoordinate coordinate)
		{
			if (Grid.Instance != null)
			{
				return Grid.Instance.GetPosition(coordinate);
			}
			return new Vector3(0.0f, 0.0f, 0.0f);
		}
	}

	public delegate void GridSourceAddedHandler(GridSource gridSource);
	public delegate void GridSourceRemovedHandler(GridSource gridSource);

	public delegate void GridObstacleAddedHandler(GridObstacle gridObstacle);
	public delegate void GridObstacleRemovedHandler(GridObstacle gridObstacle);
	public delegate void GridObstacleTransformChangedHandler(GridObstacle gridObstacle);

	/// <summary>
	/// The Grid, there can be only one instance of these at any given time.
	/// This class will contain the immutable dimensions of the grid and size of individual cell.
	/// It also contains number of methods to convert between world coordinates and grid coordinates as well as some debugging facilities.
	/// </summary>
	public class Grid : MonoBehaviour
	{
		/// <summary>
		/// Whenever grid source is added this event is fired
		/// </summary>
		public event GridSourceAddedHandler GridSourceAdded;

		/// <summary>
		/// Whenever grid source is removed this event is fired
		/// </summary>
		public event GridSourceRemovedHandler GridSourceRemoved;

		/// <summary>
		/// Whenever grid obstacle is added this event is fired
		/// </summary>
		public event GridObstacleAddedHandler GridObstacleAdded;

		/// <summary>
		/// Whenever grid obstacle is removed this event is fired
		/// </summary>
		public event GridObstacleRemovedHandler GridObstacleRemoved;

		/// <summary>
		/// Whenever non-static grid obstacle transform is changed this event is fired
		/// </summary>
		public event GridObstacleTransformChangedHandler GridObstacleTransformChanged;

		private static Grid instance;

		/// <summary>
		/// Return grid singleton instance
		/// </summary>
		public static Grid Instance
		{
			get
			{
#if UNITY_EDITOR
				if (!Application.isPlaying)
				{
					return FindObjectOfType<Grid>();
				}
#endif
				return instance;
			}
			private set
			{
				instance = value;
			}
		}

#if UNITY_EDITOR
		public delegate void GridPropertiesChangedHandler();

		public event GridPropertiesChangedHandler GridPropertiesChanged;

		private void OnValidate()
		{
			if (!Application.isPlaying)
			{
				GridPropertiesChangedHandler handler = GridPropertiesChanged;
				if (handler != null)
				{
					handler();
				}
			}
		}
		
		/// <summary>
		/// Debug drawing for editor
		/// </summary>
		public void OnDrawGizmos()
		{
			if (!debugDraw)
			{
				return;
			}

			// Find first debug processor that has DebugDraw enabled
			GridProcessor debugProcessor = null;
			GridProcessor[] processors = FindObjectsOfType<GridProcessor>();
			if (processors != null)
			{
				for (int i = 0; i < processors.Length; i++)
				{
					if (processors[i].DebugDraw)
					{
						debugProcessor = processors[i];
						break;
					}
				}
			}

			Color white = new Color(1.0f, 1.0f, 1.0f, 0.3f);

			Vector3 cellSize3d = new Vector3(cellSize, 0.01f, cellSize);
			Vector3 cellOffset = new Vector3(cellSize * 0.5f, 0.0f, cellSize * 0.5f);
			Vector2 cellPos = new Vector2();
			for (int j = 0; j < height; j++)
			{
				cellPos.y = j * cellSize;
				for (int i = 0; i < width; i++)
				{
					int offset = j * width + i;
					if (debugProcessor != null)
					{
						debugProcessor.DebugDrawCell(offset);
					}
					else
					{
						Gizmos.color = white;

						cellPos.x = i * cellSize;
						float yHeight = 0.0f;
						if (heightFieldProcessor != null && heightFieldProcessor.GridField != null)
						{
							yHeight = heightFieldProcessor.GridField.DataRead[offset] - transform.position.y;
						}
						Gizmos.DrawCube(transform.position + new Vector3(cellPos.x, yHeight, cellPos.y) + cellOffset, cellSize3d);
					}
				}
			}
		}
		 
#endif

		#region Notify methods for grid source and grid obstacle events

		private void NotifyGridSourceAdded(GridSource gridSource)
		{
			GridSourceAddedHandler handler = GridSourceAdded;
			if (handler != null)
			{
				handler(gridSource);
			}
		}

		private void NotifyGridSourceRemoved(GridSource gridSource)
		{
			GridSourceRemovedHandler handler = GridSourceRemoved;
			if (handler != null)
			{
				handler(gridSource);
			}
		}

		private void NotifyGridObstacleAdded(GridObstacle gridObstacle)
		{
			GridObstacleAddedHandler handler = GridObstacleAdded;
			if (handler != null)
			{
				handler(gridObstacle);
			}
		}

		private void NotifyGridObstacleRemoved(GridObstacle gridObstacle)
		{
			GridObstacleRemovedHandler handler = GridObstacleRemoved;
			if (handler != null)
			{
				handler(gridObstacle);
			}
		}

		private void NotifyGridObstacleTransformChanged(GridObstacle gridObstacle)
		{
			GridObstacleTransformChangedHandler handler = GridObstacleTransformChanged;
			if (handler != null)
			{
				handler(gridObstacle);
			}
		}

		#endregion

		/// <summary>
		/// Initialize instance
		/// </summary>
		void OnEnable()
		{
			if (Instance != null)
			{
				Debug.LogError("Critical error! More than one active Grid instance, this is not supported and will lead into problems!");
			}
			Instance = this;

			// Register all enabled grid sources (not enough to do it in their OnEnable() due to script update order)
			GridSource[] gridSourceArray = FindObjectsOfType<GridSource>();
			foreach (GridSource gridSource in gridSourceArray)
			{
				if (gridSource.enabled)
				{
					RegisterGridSource(gridSource);
				}
			}

			// Register all enabled grid obstacles (not enough to do it in their OnEnable() due to script update order)
			GridObstacle[] gridObstacleArray = FindObjectsOfType<GridObstacle>();
			foreach (GridObstacle gridObstacle in gridObstacleArray)
			{
				if (gridObstacle.enabled)
				{
					RegisterGridObstacle(gridObstacle);
				}
			}
		}

		/// <summary>
		/// When Unity disables this component we set the Instance to null so no-one can access us through it.
		/// </summary>
		void OnDisble()
		{
			Instance = null;
		}

		/// <summary>
		/// Get grid coordinate from position
		/// </summary>
		public GridCoordinate GetGridCoordinate(Vector3 position, float maxHeightOffset = -1.0f)
		{
			if (maxHeightOffset < 0.0f || Mathf.Abs(position.y - transform.position.y) <= maxHeightOffset)
			{
				GridCoordinate coordinate = new GridCoordinate((int)Math.Round((position.x - transform.position.x) / cellSize - 0.5f), (int)Math.Round((position.z - transform.position.z) / cellSize - 0.5f));
				if (coordinate.x >= 0 && coordinate.y >= 0 && coordinate.x < Width && coordinate.y < Height)
				{
					return coordinate;
				}
			}
			return GridCoordinate.Invalid;
		}

		/// <summary>
		/// Get cell center position from grid coordinate
		/// </summary>
		public Vector3 GetPosition(GridCoordinate coordinate)
		{
			float y = transform.position.y;
			if (heightFieldProcessor != null)
			{
				y = heightFieldProcessor.GridField.DataRead[coordinate.Offset];
			}
			return new Vector3(coordinate.x * cellSize + transform.position.x + cellSize * 0.5f, y, coordinate.y * cellSize + transform.position.z + cellSize * 0.5f);
		}

		/// <summary>
		/// Get grid coordinate with x and y fractions.
		/// </summary>
		public GridCoordinate GetGridCoordinate(Vector3 position, out float xFrac, out float yFrac)
		{
			float x = (position.x - transform.position.x) / cellSize;
			float y = (position.z - transform.position.z) / cellSize;

			int ix = (int)x;
			int iy = (int)y;

			xFrac = x - ix;
			yFrac = y - iy;

			GridCoordinate coordinate = new GridCoordinate(ix, iy);

			if (coordinate.x >= 0 && coordinate.y >= 0 && coordinate.x < Width && coordinate.y < Height)
			{
				return coordinate;
			}

			return GridCoordinate.Invalid;
		}

		/// <summary>
		/// Get cell world heights float field (if it exists). Requires height field processor set and game running.
		/// </summary>
		public GridField<float> CellWorldHeights
		{
			get
			{
				return heightFieldProcessor != null && heightFieldProcessor.GridField != null ? heightFieldProcessor.GridField : null;
			}
		}

		/// <summary>
		/// Get position from grid coordinate
		/// </summary>
		public Vector3 GetPosition(int offset)
		{
			if (offset >= 0)
			{
				float worldY = transform.position.y;
				if (heightFieldProcessor != null)
				{
					worldY = heightFieldProcessor.GridField.DataRead[offset];
				}
				int x = offset % Width;
				int y = offset / Width;
				return new Vector3(x * cellSize + transform.position.x + cellSize * 0.5f, worldY, y * cellSize + transform.position.z + cellSize * 0.5f);
			}
			return new Vector3(0.0f, 0.0f, 0.0f);
		}

		/// <summary>
		/// Get grid coordinate offset for position. If out-of-bounds then -1 is returned.
		/// </summary>
		public int GetOffset(Vector3 position)
		{
			int x = (int)Math.Round((position.x - transform.position.x) / cellSize - 0.5f);
			int y = (int)Math.Round((position.z - transform.position.z) / cellSize - 0.5f);
			return GetOffset(x, y);
		}

		/// <summary>
		/// Get neighbor offset for given offset and neighbor index. Returns -1 if neighbor is not valid (out-of-bounds).
		/// </summary>
		public int GetNeighborOffset(int offset, int neighborIndex)
		{
			if (neighborIndex >= 0 && neighborIndex <= 8)
			{
				int x = offset % Width;
				int y = offset / Width;
				x += GridCoordinate.xNeighborDir[neighborIndex];
				y += GridCoordinate.yNeighborDir[neighborIndex];
				return GetOffset(x, y);
			}
			return -1;
		}

		/// <summary>
		/// Get offset from x, y coordinates. Returns -1 if x, y are out-of-bounds.
		/// </summary>
		public int GetOffset(int x, int y)
		{
			if (x >= 0 && y >= 0 && x < Width && y < Height)
			{
				return y * Width + x;
			}
			return -1;
		}

		/// <summary>
		/// Get 2x2 grid coordinate struct with weights. Can be used to do bi-linear sampling from grid fields.
		/// </summary>
		public GridCoordinate2x2 GetGridCoordinates(Vector3 position)
		{
			GridCoordinate2x2 result;

			float x = (position.x - transform.position.x) / cellSize;
			float y = (position.z - transform.position.z) / cellSize;

			int ix = (int)x;
			int iy = (int)y;

			float xFrac = x - ix;
			float yFrac = y - iy;

			float xFracOpposite = 1.0f - xFrac;
			float yFracOpposite = 1.0f - yFrac;

			// Set offsets
			result.offset00 = GetOffset(ix, iy);
			result.offset10 = GetOffset(ix + 1, iy);
			result.offset01 = GetOffset(ix, iy + 1);
			result.offset11 = GetOffset(ix + 1, iy + 1);

			// Set weights
			result.weight00 = result.offset00 >= 0 ? xFracOpposite * yFracOpposite : 0.0f;
			result.weight10 = result.offset10 >= 0 ? xFrac * yFracOpposite : 0.0f;
			result.weight01 = result.offset01 >= 0 ? xFracOpposite * yFrac : 0.0f;
			result.weight11 = result.offset11 >= 0 ? xFrac * yFrac : 0.0f;

			// Normalize weights
			float weightSum = result.weight00 + result.weight10 + result.weight01 + result.weight11;
			result.weight00 = weightSum > 0.0f ? result.weight00 / weightSum : 0.0f;
			result.weight10 = weightSum > 0.0f ? result.weight10 / weightSum : 0.0f;
			result.weight01 = weightSum > 0.0f ? result.weight01 / weightSum : 0.0f;
			result.weight11 = weightSum > 0.0f ? result.weight11 / weightSum : 0.0f;

			return result;
		}

		/// <summary>
		/// Get grid coordinate rect from position with given half size.
		/// </summary>
		public void GetGridCoordinateRect(Vector3 position, Vector3 halfSize, out GridCoordinate start, out GridCoordinate end)
		{
			start = GridCoordinate.Invalid;
			end = GridCoordinate.Invalid;

			int startX = (int)Mathf.Round((position.x - transform.position.x - halfSize.x) / cellSize - 0.5f);
			int startY = (int)Mathf.Round((position.z - transform.position.z - halfSize.z) / cellSize - 0.5f);
			if (startX < Width && startY < Height)
			{
				int endX = (int)Mathf.Round((position.x - transform.position.x + halfSize.x) / cellSize - 0.5f);
				int endY = (int)Mathf.Round((position.z - transform.position.z + halfSize.z) / cellSize - 0.5f);
				if (endX >= 0 && endY >= 0)
				{
					start = new GridCoordinate(Mathf.Max(startX, 0), Mathf.Max(startY, 0));
					end = new GridCoordinate(Mathf.Min(endX, Width - 1), Mathf.Min(endY, Height - 1));
				}
			}
		}

		/// <summary>
		/// Get valid grid coordinate offsets within a circle specified by position and radius. Height (y axis) is ignored.
		/// </summary>
		public List<int> GetGridOffsets(Vector3 position, float radius)
		{
			List<int> coordinates = new List<int>();

			GridCoordinate start = GridCoordinate.Invalid;
			GridCoordinate end = GridCoordinate.Invalid;
			GetGridCoordinateRect(position, new Vector3(radius, 0.0f, radius), out start, out end);

			if (start.Valid && end.Valid)
			{
				float sqrRadius = radius * radius;
				float halfCellSize = cellSize * 0.5f;

				for (int y = start.y; y <= end.y; y++)
				{
					for (int x = start.x; x <= end.x; x++)
					{
						Vector3 cellCenter = new Vector3(x * cellSize + transform.position.x, transform.position.y, y * cellSize + transform.position.z);
						Vector3 toPosition = position - cellCenter;
						toPosition.x = Mathf.Clamp(toPosition.x, -halfCellSize, halfCellSize);
						toPosition.z = Mathf.Clamp(toPosition.z, -halfCellSize, halfCellSize);

						if ((cellCenter + toPosition - position).sqrMagnitude <= sqrRadius)
						{
							// Cell borders are within the circle
							coordinates.Add(y * width + x);
						}
					}
				}
			}
			
			return coordinates;
		}

		/// <summary>
		/// Width of the grid in cell count
		/// </summary>
		public int Width
		{
			get
			{
				return width;	
			}
		}

		/// <summary>
		/// Height of the grid in cell count
		/// </summary>
		public int Height
		{
			get
			{
				return height;
			}
		}

		/// <summary>
		/// Number of cells in the grid
		/// </summary>
		public int NumCells
		{
			get
			{
				return width * height;
			}
		}

		/// <summary>
		/// Cell size as 3D vector for debug drawing purposes
		/// </summary>
		public Vector3 CellSize2d
		{
			get
			{
				return new Vector3(cellSize, 0.01f, cellSize);
			}
		}

		/// <summary>
		/// Cell size (uniform)
		/// </summary>
		public float CellSize
		{
			get
			{
				return cellSize;
			}
		}

		/// <summary>
		/// Register grid source to be managed by this manager.
		/// </summary>
		public void RegisterGridSource(GridSource gridSource)
		{
			if (!gridSources.Contains(gridSource))
			{
				gridSources.Add(gridSource);

				NotifyGridSourceAdded(gridSource);
			}
		}

		/// <summary>
		/// Unregister grid source from being managed by this manager.
		/// </summary>
		public void UnregisterGridSource(GridSource gridSource)
		{
			if (gridSources.Remove(gridSource))
			{
				NotifyGridSourceRemoved(gridSource);
			}
		}

		/// <summary>
		/// Get all registered grid sources
		/// </summary>
		public List<GridSource> GridSources
		{
			get
			{
				return gridSources;	
			}
		}

		/// <summary>
		/// Register grid obstacle to be managed by this manager.
		/// </summary>
		public void RegisterGridObstacle(GridObstacle gridObstacle)
		{
			if (!gridObstacles.Contains(gridObstacle))
			{
				gridObstacles.Add(gridObstacle);
				if (!gridObstacle.IsStatic)
				{
					gridObstacle.ObstacleChanged += OnGridObstacleChanged;
				}

				NotifyGridObstacleAdded(gridObstacle);
			}
		}

		/// <summary>
		/// Unregister grid obstacle from being managed by this manager.
		/// </summary>
		public void UnregisterGridObstacle(GridObstacle gridObstacle)
		{
			if (gridObstacles.Remove(gridObstacle))
			{
				NotifyGridObstacleRemoved(gridObstacle);
			}
			if (!gridObstacle.IsStatic)
			{
				gridObstacle.ObstacleChanged -= OnGridObstacleChanged;
			}
		}

		private void OnGridObstacleChanged(GridObstacle gridObstacle)
		{
			NotifyGridObstacleTransformChanged(gridObstacle);
		}

		/// <summary>
		/// Get all registered grid obstacles
		/// </summary>
		public List<GridObstacle> GridObstacles
		{
			get
			{
				return gridObstacles;
			}
		}

		// Grid setup
		[SerializeField, Tooltip("Width of the grid in number of cells.")]
		private int width = 64;
		[SerializeField, Tooltip("Height of the grid in number of cells.")]
		private int height = 64;
		[SerializeField, Tooltip("Size of one grid cell.")]
		private float cellSize = 1.0f;

		[SerializeField, Tooltip("Optional height field processor to determine the height of each cell in the grid.")]
		private HeightFieldProcessor heightFieldProcessor;

		// Debug setup
		[SerializeField, Tooltip("Is debug drawing enabled.")]
		private bool debugDraw = true;

		// List of registered grid sources
		private List<GridSource> gridSources = new List<GridSource>();

		// List of registered grid obstacles
		private List<GridObstacle> gridObstacles = new List<GridObstacle>();
	}
}