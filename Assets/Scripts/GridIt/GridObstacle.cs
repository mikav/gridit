﻿using UnityEngine;
using System.Collections;

namespace GridIt
{
	public delegate void ObstacleChangedHandler(GridObstacle obstacle);

	public class GridObstacle : MonoBehaviour
	{
		public event ObstacleChangedHandler ObstacleChanged;

		public Vector3 AABBMin
		{
			get;
			private set;
		}

		public Vector3 AABBMax
		{
			get;
			private set;
		}

		public Vector3 LocalSize
		{
			get;
			private set;
		}

		public Matrix4x4 WorldToLocal
		{
			get;
			private set;
		}

		public Vector3 PreviousAABBMin
		{
			get;
			private set;
		}

		public Vector3 PreviousAABBMax
		{
			get;
			private set;
		}

		public Vector3 PreviousLocalSize
		{
			get;
			private set;
		}

		public Matrix4x4 PreviousWorldToLocal
		{
			get;
			private set;
		}

		private void UpdateObstacleBounds()
		{
			BoxCollider boxCollider = GetComponent<BoxCollider>();
			if (boxCollider != null)
			{
				PreviousAABBMin = AABBMin;
				PreviousAABBMax = AABBMax;
				PreviousLocalSize = LocalSize;
				PreviousWorldToLocal = WorldToLocal;

				AABBMin = boxCollider.bounds.min;
				AABBMax = boxCollider.bounds.max;
				LocalSize = boxCollider.size;
				WorldToLocal = transform.worldToLocalMatrix;
			}
#if UNITY_EDITOR
			else
			{
				if (!warningPrinted)
				{
					warningPrinted = true;
					Debug.LogError("GridObstacle requires to have BoxCollider component in the same game object. Game object '" + gameObject.name + "' is missing BoxCollider!");
				}
			}
#endif
		}

		private void OnEnable()
		{
			warningPrinted = false;

			UpdateObstacleBounds();

			if (Grid.Instance != null)
			{
				Grid.Instance.RegisterGridObstacle(this);
			}
		}

		private void OnDisable()
		{
			if (Grid.Instance != null)
			{
				Grid.Instance.UnregisterGridObstacle(this);
			}
		}

		private void NotifyObstacleChanged()
		{
			ObstacleChangedHandler handler = ObstacleChanged;
			if (handler != null)
			{
				handler(this);
			}
		}

		void LateUpdate()
		{
			if (!isStatic && transform.hasChanged)
			{
				UpdateObstacleBounds();

				transform.hasChanged = false;

				NotifyObstacleChanged();
			}
		}

		/// <summary>
		/// Is this grid obstacle static. Static means GridIt won't react to changes in its transform but will reflect change when it becomes enabled or disabled.
		/// </summary>
		public bool IsStatic
		{
			get
			{
				return isStatic;	
			}
		}

#if UNITY_EDITOR
		private bool warningPrinted = false;
#endif

		[SerializeField, Tooltip("Is this a static obstacle, in other words after initialization it won't move or become inactive. By default all obstacles are static and not expected to change. Note! If you delete or disable this obstacle it won't affect the ObstacleProcessor but moving this will have no effect.")]
		private bool isStatic;
	}
}