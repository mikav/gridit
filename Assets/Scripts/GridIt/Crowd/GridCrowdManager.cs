using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace GridIt
{
	/// <summary>
	/// Used to cache movement vectors calculated from the distance field.
	/// </summary>
	public struct MovementCacheEntry
	{
		public static MovementCacheEntry Empty = new MovementCacheEntry() { x = 0.0f, y = 0.0f, distanceAtCoordinate = 0.0f, frame = -1 };

		public float x;
		public float y;
		public float distanceAtCoordinate;
		public int frame;
	}

	/// <summary>
	/// Helper class to setup which distance field to use for different crowd actors according to their grid source layers.
	/// </summary>
	[Serializable]
	public struct DistanceFieldSetup
	{
		[Tooltip("Identifier for the distance field setup, crowd actors can provide any ID they want which maps to a processor specified in the grid crowd manager.")]
		public int id;
		[Tooltip("The distance field to be used for the specified layer.")]
		public DistanceFieldProcessor processor;
	}

	/// <summary>
	/// Manager class to perform grid crowd actor movement calculations based on distance fields, obstacle field and occupancy field.
	/// It will perform basic movement calculation and local avoidance using the setup from each crowd actor.
	/// Crowd actor can also control whether it should be moving or avoiding and will perform the actual movement based on the input from this manager.
	/// There can be only one crowd manager active at any given time.
	/// This class will consume a lot of processing power depending on the amount of crowd actors it manages.
	/// </summary>
	public class GridCrowdManager : MonoBehaviour
	{
		private static GridCrowdManager instance;

		public static GridCrowdManager Instance
		{
			get
			{
#if UNITY_EDITOR
				if (!Application.isPlaying)
				{
					return FindObjectOfType<GridCrowdManager>();
				}
#endif
				return instance;
			}
			private set
			{
				instance = value;
			}
		}

		/// <summary>
		/// Initialize instance
		/// </summary>
		private void Awake()
		{
			if (Instance != null)
			{
				Debug.LogError("Critical error! More than one active GridCrowdManager instance, this is not supported and will lead into problems!");
			}
			Instance = this;

			// Register all enabled crowd actors
			GridCrowdActor[] crowdActorsArray = FindObjectsOfType<GridCrowdActor>();
			foreach (GridCrowdActor crowdActor in crowdActorsArray)
			{
				if (crowdActor.enabled)
				{
					RegisterCrowdActor(crowdActor);
				}
			}
		}

		/// <summary>
		/// When Unity destroys this component we set the Instance to null so no-one can access us through it.
		/// </summary>
		private void OnDestroy()
		{
			Instance = null;
		}

		/// <summary>
		/// Register crowd actor to be managed by this manager.
		/// </summary>
		public void RegisterCrowdActor(GridCrowdActor actor)
		{
			if (!crowdActors.Contains(actor))
			{
				int index = crowdActors.BinarySearch(actor);
				if (index < 0)
				{
					crowdActors.Insert(~index, actor);
				}
				else
				{
					crowdActors.Insert(index, actor);
				}
			}
		}

		/// <summary>
		/// Unregister crowd actor from being managed by this manager.
		/// </summary>
		public void UnregisterCrowdActor(GridCrowdActor actor)
		{
			crowdActors.Remove(actor);
		}

		/// <summary>
		/// Get distance field processor for specified layer.
		/// </summary>
		protected virtual DistanceFieldProcessor GetDistanceFieldProcessor(int id)
		{
			foreach (DistanceFieldSetup setup in distanceFieldSetup)
			{
				if (setup.id == id)
				{
					return setup.processor;
				}
			}
			return null;
		}

		/// <summary>
		/// Helper function to add movement value to the movementDirection based on 3 x 3 sample from the distance field.
		/// This should (and hopefully will) be inlined. It utilizes directionCacheField to avoid recalculation if multiple crowd actors happen to be close to each other.
		/// </summary>
		private void AddToMovementDirection(ref Vector3 movementDirection, ref float distanceSumAtCoordinate, GridField<float> distanceField, GridField<int> obstacleField, int offset, float weight)
		{
			if (directionCacheField.DataRead[offset].frame != Time.frameCount)
			{
				float distanceAtCoordinate = 0.0f;
				Vector3 movementVector = DistanceFieldProcessor.GetDirection(distanceField, obstacleField, offset, out distanceAtCoordinate);
				directionCacheField.DataRead[offset].frame = Time.frameCount;
				directionCacheField.DataRead[offset].x = movementVector.x;
				directionCacheField.DataRead[offset].y = movementVector.z;
				directionCacheField.DataRead[offset].distanceAtCoordinate = distanceAtCoordinate;
				movementDirection += movementVector * weight;
				distanceSumAtCoordinate += distanceAtCoordinate * weight; 
			}
			else
			{
				MovementCacheEntry entry = directionCacheField.DataRead[offset];
				movementDirection.x += entry.x * weight;
				movementDirection.z += entry.y * weight;
				distanceSumAtCoordinate += entry.distanceAtCoordinate * weight; 
			}
		}

		/// <summary>
		/// Update for the Unity component.
		/// O(n) / O(n^2) where n is the number of crowd actors, this is influenced by whether avoidance is enabled and what are the OccupancyProcessor settings for maximum actors per cell.
		/// Practically it won't be n^2 due to the optimization data provided by the OccupancyProcessor which will cap the number of actors to check when calculating avoidance.
		/// </summary>
		private void Update()
		{
			// Check if crowd actors need re-sorting
			if (shouldSortActors)
			{
				crowdActors.Sort();
			}

			List<GridSource> gridSources = Grid.Instance.GridSources;

			// Cache some values
			GridField<float> currentDistanceField = null;
			int currentDistanceFieldId = -1;

			if (directionCacheField == null && useBilinearInterpolation)
			{
				// Create directionCacheField if using bilinear interpolation (not double-buffered)
				directionCacheField = new GridField<MovementCacheEntry>(Grid.Instance.Width, Grid.Instance.Height, MovementCacheEntry.Empty, false);
			}

			GridField<int> obstacleField = obstacleProcessor != null ? obstacleProcessor.GridField : null;
			float cellSize = Grid.Instance.CellSize;

			GridField<byte> occupancyCountField = occupancyProcessor != null ? occupancyProcessor.CountGridField : null;
			GridField<short> occupancyIndexField = occupancyProcessor != null ? occupancyProcessor.GridField : null;
			int occupancyMaxNumberOfActorsPerCell = occupancyProcessor != null ? occupancyProcessor.MaxNumberOfActorsPerCell : 0;

			// Take copy (so we can delete actors while processing them)
			GridCrowdActor[] actorsToProcess = crowdActors.ToArray();

			// Iterate all crowd actors
			foreach (GridCrowdActor crowdActor in actorsToProcess)
			{
				if (!crowdActor.WantsToMove && !crowdActor.WantsToAvoid)
				{
					// This crowd actor does not want to move -> Skip all calculations and call to the Move() function.
					continue;
				}

				if (crowdActor.DistanceFieldId != currentDistanceFieldId)
				{
					// New layer, crowd actors are sorted per layer so here we find out that there is a new layer which needs a new distance field processor
					currentDistanceFieldId = crowdActor.DistanceFieldId;
					DistanceFieldProcessor processor = GetDistanceFieldProcessor(currentDistanceFieldId);
					currentDistanceField = processor != null ? processor.GridField : null;
				}

				Vector3 actorPosition = crowdActor.transform.position;
				int actorOffset = Grid.Instance.GetOffset(actorPosition);

				Vector3 movementDirection = new Vector3(0.0f, 0.0f, 0.0f);
				Vector3 avoidance = new Vector3(0.0f, 0.0f, 0.0f);
				float distanceAtCoordinate = 0.0f;

				// Check if we can calculate movement vector using the distance field
				if (currentDistanceField != null && crowdActor.WantsToMove)
				{
					if (useBilinearInterpolation)
					{
						// Get 2 x 2 cell offsets and their weights based on world position
						GridCoordinate2x2 sampleCoordinates = Grid.Instance.GetGridCoordinates(actorPosition);

						// Calculate movement vector for crowd actor based on the distance field
						movementDirection = new Vector3(0.0f, 0.0f, 0.0f);

						// Process each potential movement vector
						if (sampleCoordinates.offset00 >= 0)
						{
							AddToMovementDirection(ref movementDirection, ref distanceAtCoordinate, currentDistanceField, obstacleField, sampleCoordinates.offset00, sampleCoordinates.weight00);
						}
						if (sampleCoordinates.offset10 >= 0)
						{
							AddToMovementDirection(ref movementDirection, ref distanceAtCoordinate, currentDistanceField, obstacleField, sampleCoordinates.offset10, sampleCoordinates.weight10);
						}
						if (sampleCoordinates.offset01 >= 0)
						{
							AddToMovementDirection(ref movementDirection, ref distanceAtCoordinate, currentDistanceField, obstacleField, sampleCoordinates.offset01, sampleCoordinates.weight01);
						}
						if (sampleCoordinates.offset11 >= 0)
						{
							AddToMovementDirection(ref movementDirection, ref distanceAtCoordinate, currentDistanceField, obstacleField, sampleCoordinates.offset11, sampleCoordinates.weight11);
						}
					}
					else
					{
						// Just do one sample, since it is unlikely that multiple actors share the same grid cell in the case of ever sampling only once cell the use of cache is skipped here.
						movementDirection = DistanceFieldProcessor.GetDirection(currentDistanceField, obstacleField, actorOffset, out distanceAtCoordinate);
					}
				}

				// Check if we can calculate avoidance vector using the occupancy field
				if (occupancyCountField != null && crowdActor.WantsToAvoid)
				{
					// Calculate avoidance vector for crowd actor
					if (actorOffset >= 0)
					{
						int count = occupancyCountField.DataRead[actorOffset];
						for (int i = 0; i < count; i++)
						{
							int sourceOffset = occupancyIndexField.DataRead[actorOffset * occupancyMaxNumberOfActorsPerCell + i];
							if (sourceOffset >= 0 && sourceOffset < gridSources.Count)
							{
								GridSource otherActor = gridSources[sourceOffset];
								if (otherActor == null || otherActor == crowdActor)
								{
									// Us or null -> Continue
									continue;
								}

								Vector3 fromOther = actorPosition - otherActor.transform.position;

								float sqrDistance = fromOther.sqrMagnitude;
								if (sqrDistance > 0.0f)
								{
									// Some distance apart -> Check if within avoidance distance threshold
									if (sqrDistance <= crowdActor.AvoidanceSqrDistance)
									{
										float distance = Mathf.Sqrt(sqrDistance);
										
										// Normalize
										fromOther /= distance;

										// Avoidance magnitude is inversely proportional to the square distance
										fromOther /= sqrDistance;

										avoidance += fromOther;
									}
								}
								else
								{
									// Exactly on top of each other -> Random direction
									Vector2 randomVector = UnityEngine.Random.insideUnitCircle;
									avoidance += new Vector3(randomVector.x, 0.0f, randomVector.y);
								}

								// Notify other actor (in different layer) is in proximity
								if (crowdActor.Layer != otherActor.Layer && sqrDistance <= crowdActor.ProximitySqrDistance)
								{
									crowdActor.InProximity(otherActor as GridCrowdActor);
								}
							}
						}

						// Clamp avoidance to be maximum of half cellsize during one frame (this avoids skipping over obstacles or having overly large velocities caused by actors getting very close to each other).
						avoidance = Vector3.ClampMagnitude(avoidance, cellSize * 0.5f / crowdActor.AvoidanceStrength / Time.deltaTime);
						avoidance.y = 0.0f;
					}
				}

				// Calculate final movement delta
				Vector3 avoidanceDelta = avoidance * crowdActor.AvoidanceStrength * Time.deltaTime;
				Vector3 movementDelta = movementDirection * crowdActor.CurrentMoveSpeedPerSecond * Time.deltaTime;

				// Check if the newPosition would lead us into a blocked cell using the obstacleField
				Vector3 newPosition = actorPosition + movementDelta + avoidanceDelta;
				int newOffset = Grid.Instance.GetOffset(newPosition);
				if (newOffset < 0 || (obstacleField != null && obstacleField.DataRead[newOffset] != 0))
				{
					// We'd get into blocked cell or outside of the grid bounds -> Clear avoidance movement since distance field sampling cannot lead into blocked cells or out-of-bounds
					avoidanceDelta = new Vector3(0.0f, 0.0f, 0.0f);

					// Check if basic movement would lead us into trouble
					newPosition = actorPosition + movementDelta;
					newOffset = Grid.Instance.GetOffset(newPosition);
					if (newOffset < 0 || (obstacleField != null && obstacleField.DataRead[newOffset] !=0))
					{
						// Still in trouble -> Disable bi-linear interpolation temporarily for this actor
						if (useBilinearInterpolation)
						{
							movementDirection = DistanceFieldProcessor.GetDirection(currentDistanceField, obstacleField, actorOffset, out distanceAtCoordinate);
							movementDelta = movementDirection * crowdActor.CurrentMoveSpeedPerSecond * Time.deltaTime;
						}
						else
						{
							// Bi-linear interpolation not used -> Stay in place (this leads into actor getting stuck -> should not happen)
							movementDelta = new Vector3(0.0f, 0.0f, 0.0f);
						}
					}
				}

				// Set the position of the crowd actor
				crowdActor.Move(movementDelta, avoidanceDelta, distanceAtCoordinate);
			}
		}

		/// <summary>
		/// Request re-sort of crowd actors (based on their DistanceFieldId). This is called by the DistanceFieldId accessor in the GridCrowdActor.
		/// Do not call this method.
		/// </summary>
		public void RequestCrowdActorSort()
		{
			shouldSortActors = true;
		}

		[SerializeField, Tooltip("Occupancy processor to be used for crowd avoidance.")]
		private OccupancyProcessor occupancyProcessor;

		[SerializeField, Tooltip("Obstacle processor to be used for crowd collision detection against solid obstacles.")]
		private ObstacleProcessor obstacleProcessor;

		[SerializeField, Tooltip("Distance field setup for different grid source layers.")]
		private List<DistanceFieldSetup> distanceFieldSetup;

		[SerializeField, Tooltip("Should each crowd actor take 2 x 2 (4) movement direction samples and perform bilinear interpolation to find smoother movement vector. This will add performance cost.")]
		private bool useBilinearInterpolation = true;

		// This is used to cache direction samples when using bilinear interpolation. 
		// Each actor will perform 4 calculations and it is likely that with large enough crowd the directions would be calculated multiple times for some of the cells.
		private GridField<MovementCacheEntry> directionCacheField;

		private List<GridCrowdActor> crowdActors = new List<GridCrowdActor>();

		private bool shouldSortActors = false;
	}
}