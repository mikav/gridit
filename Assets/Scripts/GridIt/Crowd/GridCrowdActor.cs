﻿using UnityEngine;
using System.Collections;
using System;

namespace GridIt
{
	/// <summary>
	/// Grid crowd actor component used by GridCrowdManager. This component is used to specify the properties of individual crowd actor such as movement speed and avoidance strength.
	/// It is also used to allow sorting of crowd actors according to the GridSource (base class) layer.
	/// </summary>
	public class GridCrowdActor : GridSource, IComparable<GridCrowdActor>
	{
		/// <summary>
		/// IComparable implementation, based on DistanceFieldId
		/// </summary>
		public int CompareTo(GridCrowdActor other)
		{
			return DistanceFieldId.CompareTo(other.DistanceFieldId);
		}

		/// <summary>
		/// Distance field id to be used (this refers to the setup in GridCrowdManager).
		/// When this is updated (to a different value) it will notify crowd manager that it needs to perform a re-sort of the crowd actors. It will do this in the beginning of its next update.
		/// </summary>
		public int DistanceFieldId
		{
			get
			{
				return distanceFieldId;
			}
			set
			{
				if (distanceFieldId != value)
				{
					distanceFieldId = value;
					GridCrowdManager.Instance.RequestCrowdActorSort();
				}
			}
		}

		/// <summary>
		/// Current distance at coordinate, updated by GridCrowdManager when calling Move()
		/// </summary>
		public float CurrentDistanceAtCoordinate
		{
			get;
			protected set;
		}

		/// <summary>
		/// Current movement speed per second (if current distance to target is less than 5 units then speed will slowly interpolate towards 0 until at target).
		/// </summary>
		public float CurrentMoveSpeedPerSecond
		{
			get
			{
				return currentMoveSpeedPerSecond;
			}
		}

		/// <summary>
		/// Avoidance strength (scale)
		/// </summary>
		public float AvoidanceStrength
		{
			get
			{
				return avoidanceStrength;
			}
		}

		/// <summary>
		/// Avoidance square distance
		/// </summary>
		public float AvoidanceSqrDistance
		{
			get;
			protected set;
		}

		/// <summary>
		/// Avoidance distance
		/// </summary>
		public float AvoidanceDistance
		{
			get
			{
				return avoidanceDistance;
			}
		}

		/// <summary>
		/// Does this crowd actor want to move. If this is false then crowd manager will skip the movement calculations for this actor and won't call Move() if also WantsToAvoid is false.
		/// You can override this method in a derived class to further control the behavior of individual crowd actors.
		/// By default this always returns true.
		/// </summary>
		public virtual bool WantsToMove
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Does this crowd actor want to avoid others. If this is false then crowd manager will skip the avoidance calculations for this actor and won't call Move() if also WantsToMove is false.
		/// You can override this method in a derived class to further control the behavior of individual crowd actors.
		/// By default this always returns true.
		/// </summary>
		public virtual bool WantsToAvoid
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Perform movement based on the calculations done by GridCrowdManager.
		/// You can override this method in a derived class to further influence the movement.
		/// </summary>
		public virtual void Move(Vector3 movementDelta, Vector3 avoidanceDelta, float currentDistanceAtCoordinate)
		{
			float targetSpeed = moveSpeedPerSecond * Mathf.Clamp(CurrentDistanceAtCoordinate / 2.0f, 0.25f, 1.0f);
			if (currentMoveSpeedPerSecond < targetSpeed)
			{
				currentMoveSpeedPerSecond = Math.Min(targetSpeed, currentMoveSpeedPerSecond + acceleration * Time.deltaTime);
			}
			else if (currentMoveSpeedPerSecond > targetSpeed)
			{
				currentMoveSpeedPerSecond = Math.Max(targetSpeed, currentMoveSpeedPerSecond - acceleration * Time.deltaTime);
			}

			transform.position += (movementDelta + avoidanceDelta);
			CurrentDistanceAtCoordinate = currentDistanceAtCoordinate;
		}

		/// <summary>
		/// This method will be called when grid source activated (from Unity Awake() method).
		/// Override this to perform any custom initialization. Do NOT implement Awake() method on derived classes, also remember to call base.Init().
		/// </summary>
		public override void Init()
		{
			base.Init();

			distanceFieldId = Layer;

			if (GridCrowdManager.Instance != null)
			{
				// Depending on the script execution order the manager might not have been created yet. It will add all crowd actors in its own Awake that have not been registered already.
				GridCrowdManager.Instance.RegisterCrowdActor(this);
			}

			CurrentHealth = health;

			AvoidanceSqrDistance = avoidanceDistance * avoidanceDistance;
			ProximitySqrDistance = proximityDistance * proximityDistance;
		}

		/// <summary>
		/// This method will be called when grid source is being destroyed (from unity OnDestroy() method).
		/// Override this to perform any custom uninitialization. Do NOT implement OnDestoy() method on derived classes, also remember to call base.Uninit().
		/// </summary>
		public override void Uninit()
		{
			base.Uninit();

			if (GridCrowdManager.Instance != null)
			{
				// Depending on the script execution order the manager might have been destroyed already
				GridCrowdManager.Instance.UnregisterCrowdActor(this);
			}
		}

		/// <summary>
		/// Called by GridCrowdManager when other actor is within SqrProximityDistance.
		/// </summary>
		public virtual void InProximity(GridCrowdActor otherActor)
		{
			if (otherActor != null)
			{
				// Different "faction"
				otherActor.Damage(damagePerSecond * Time.deltaTime);
			}
		}

		/// <summary>
		/// Square reporting distance to another crowd actor
		/// </summary>
		public float ProximitySqrDistance
		{
			get;
			protected set;
		}

		/// <summary>
		/// Is this crowd actor alive (CurrentHealth > 0.0f)
		/// </summary>
		public bool Alive
		{
			get
			{
				return CurrentHealth > 0.0f;
			}
		}

		/// <summary>
		/// Damage this crowd actor. If health goes to 0 or below this crowd actor is killed (game object destroyed)
		/// </summary>
		public void Damage(float amount)
		{
			CurrentHealth -= amount;
			if (CurrentHealth <= 0.0f)
			{
				Destroy(gameObject);
			}
		}
						
		/// <summary>
		/// Current health
		/// </summary>
		public float CurrentHealth
		{
			get;
			protected set;
		}

		private float currentMoveSpeedPerSecond;

		[SerializeField, Tooltip("Crowd actor health.")]
		private float health = 100.0f;

		[SerializeField, Tooltip("Crowd actor damage per second.")]
		private float damagePerSecond = 50.0f;

		[SerializeField, Tooltip("Crowd actor movement speed per second.")]
		private float moveSpeedPerSecond = 1.0f;

		[SerializeField, Tooltip("Avoidance strength.")]
		private float avoidanceStrength = 1.0f;

		[SerializeField, Tooltip("Square reporting distance to another crowd actor")]
		private float proximityDistance = 1.0f;

		[SerializeField, Tooltip("Avoidance distance between actors")]
		private float avoidanceDistance = 0.6f;

		[SerializeField, Tooltip("Avoidance distance between actors")]
		private float acceleration = 5.0f;
	
		private int distanceFieldId = -1;
	}
}