﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridIt
{
	/// <summary>
	/// Calculate height-field raycasting against the world with given raycast layer configuration. It is assumed to be updated very infrequently, potentially only once at init time.
	/// Each cell will contain a signed float offset to the grid transform along Y-axis.
	/// This is O(n) where n is the number of grid cells. Each cell will require one raycast.
	/// </summary>
	public class HeightFieldProcessor : GridProcessor
	{
		private Vector3 lastGridPosition;

		/// <summary>
		/// Create grid field and setup for "not processing" state.
		/// </summary>
		public override void InitProcessor()
		{
			GridField = new GridField<float>(Grid.Instance.Width, Grid.Instance.Height, 0.0f);
			currentIndex = -1;
			needsUpdate = true;
		}

		/// <summary>
		/// This processor will have to be specifically told to update (expensive and expecting height field to remain static)
		/// </summary>
		public override bool NeedsUpdate
		{
			get
			{
				bool gridMoved = Grid.Instance != null && (Grid.Instance.transform.position - lastGridPosition).sqrMagnitude > 0.0f;
				return needsUpdate || gridMoved;
			}
		}

		/// <summary>
		/// Request update of the height field
		/// </summary>
		public void RequestUpdate()
		{
			needsUpdate = true;
		}

		/// <summary>
		/// Prepare for new update iteration
		/// </summary>
		public override void UpdateIterationStart()
		{
			currentIndex = 0;
		}

		/// <summary>
		/// Process next iteration, if returns 'false' then further Updates are needed, 'true' is returned when processing is completed fully.
		/// </summary>
		public override bool UpdateIteration()
		{
			Vector3 down = new Vector3(0.0f, -1.0f, 0.0f);

			float halfRaycastDistance = raycastDistance * 0.5f;
			Vector3 upOffset = -down * halfRaycastDistance;
			int numCells = Grid.Instance.NumCells;
			float gridY = Grid.Instance.transform.position.y;
			for (int i = 0; (cellIterationsPerFrame < 0 || i < cellIterationsPerFrame) && currentIndex < numCells; i++)
			{
				Vector3 cellInitialPosition = (new GridCoordinate(currentIndex)).ToVector3();
				cellInitialPosition.y = gridY;
				RaycastHit hitInfo;
				if (Physics.Raycast(cellInitialPosition + upOffset, down, out hitInfo, raycastDistance, raycastLayer))
				{
					GridField.DataWrite[currentIndex] = hitInfo.point.y;
				}
				else
				{
					GridField.DataWrite[currentIndex] = cellInitialPosition.y - upOffset.y;
				}

				currentIndex++;
				if (currentIndex >= numCells)
				{
					// We're done
					return true;
				}
			}

			// Need to continue calculations
			return false;
		}

		/// <summary>
		/// Update iteration done
		/// </summary>
		public override void UpdateIterationEnd()
		{
			// Calculations done -> Flip buffers so we can read results
			needsUpdate = false;
			GridField.Flip();
			if (Grid.Instance != null)
			{
				lastGridPosition = Grid.Instance.transform.position;
			}
		}

		/// <summary>
		/// Is processing still on-going
		/// </summary>
		public override bool UpdateIterationInProgress
		{
			get
			{
				return currentIndex >= 0 && currentIndex < Grid.Instance.NumCells;
			}
		}

		/// <summary>
		/// Data GridField where results are written
		/// </summary>
		public GridField<float> GridField
		{
			get;
			private set;
		}

		private bool needsUpdate;

		// Update setup
		private int currentIndex;

		[SerializeField, Tooltip("How many cells to process per frame. Default is -1 which will process all cells at once.")]
		private int cellIterationsPerFrame = -1;

		[SerializeField, Tooltip("Raycast distance, ray will start at grid transform level -raycastDistance / 2.")]
		private float raycastDistance = 1000.0f;

		[SerializeField, Tooltip("Raycast layer mask to be used.")]
		private int raycastLayer = Physics.DefaultRaycastLayers;
	}
}
