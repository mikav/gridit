using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace GridIt
{
	/// <summary>
	/// Helper class for open list entries, this enables sorting.
	/// </summary>
	public class OpenListEntry : IComparable<OpenListEntry>
	{
		public OpenListEntry(int inOffset, float inDistance)
		{
			offset = inOffset;
			distance = inDistance;
		}

		public int CompareTo(OpenListEntry other)
		{
 			return -distance.CompareTo(other.distance);
		}
	
		public int offset;
		public float distance;
	}

	/// <summary>
	/// Utility class to create sorted openlist queue. This is required to support variable costs between cells.
	/// </summary>
	public class OpenList
	{
		public OpenList()
		{
			entries = new List<OpenListEntry>();
		}

		/// <summary>
		/// Add new entry to open list (sort immediately) -> O(n)
		/// </summary>
		public void Enqueue(int offset, float distance)
		{
			OpenListEntry entry = new OpenListEntry(offset, distance);
			int index = entries.BinarySearch(entry);
			if (index < 0)
			{
				entries.Insert(~index, entry);
			}
			else
			{
				entries.Insert(index, entry);
			}
		}

		/// <summary>
		/// Dequeue from the end of list and remove entry -> O(1)
		/// </summary>
		public OpenListEntry Dequeue()
		{
			if (entries.Count > 0)
			{
				OpenListEntry entry = entries[entries.Count - 1];
				entries.RemoveAt(entries.Count - 1);
				return entry;
			}

			return null;
		}

		/// <summary>
		/// How many entries in list
		/// </summary>
		public int Count
		{
			get
			{
				return entries != null ? entries.Count : 0;
			}
		}

		public List<OpenListEntry> entries;
	}

	/// <summary>
	/// Calculates a distance field from list of sources. ObstacleProcessor can be given to specify obstacles but is not required.
	/// Each cell in the resulting grid field contains distance to nearest source. 
	/// It will not be the "real" shortest distance due to resolution limitation in the grid while it will generate the shortest path in terms of grid cell traversal.
	/// Processing is O(n) regardless of the amount of sources where n is the number of grid cells.
	/// </summary>
	public class DistanceFieldProcessor : GridProcessor
	{
		public static float sDistanceNotSet = 10000.0f;

		/// <summary>
		/// Process immediately and return resulting GridField. If distanceField parameter is null a new GridField is created.
		/// </summary>
		public static GridField<float> ImmediateProcess(GridField<float> distanceField)
		{
			DistanceFieldProcessor processor = new DistanceFieldProcessor();
			processor.InitProcessor(distanceField, -1);
			processor.UpdateFullIterationCycle();
			return processor.GridField;
		}

		/// <summary>
		/// Helper for ImmediateProcess initialization where none of the members can be set in the scene setup.
		/// </summary>
		private void InitProcessor(GridField<float> distanceField, int cellIterationsPerFrame)
		{
			this.cellIterationsPerFrame = cellIterationsPerFrame;
			if (distanceField != null)
			{
				distanceField.Flip();
			}
			GridField = distanceField != null ? distanceField : new GridField<float>(Grid.Instance.Width, Grid.Instance.Height, sDistanceNotSet);

			ReadMaxDistance = 1.0f;
			WriteMaxDistance = 1.0f;
		}

		/// <summary>
		/// Create grid field and setup for "not processing" state.
		/// </summary>
		public override void InitProcessor()
		{
			GridField = new GridField<float>(Grid.Instance.Width, Grid.Instance.Height, sDistanceNotSet);

			ReadMaxDistance = 1.0f;
			WriteMaxDistance = 1.0f;
		}

		/// <summary>
		/// Data GridField where results are written
		/// </summary>
		public GridField<float> GridField
		{
			get;
			private set;
		}

		/// <summary>
		/// Get direction from grid coordinate by sampling all of the neighbors and using the gradient to figure out direction towards the smaller distances.
		/// </summary>
		public static Vector3 GetDirection(GridField<float> distanceField, GridField<int> obstacleField, int offset, out float distanceAtCoordinate, bool useDiagonalDirections = true)
		{
			distanceAtCoordinate = 0.0f;
			Grid grid = Grid.Instance;
			Vector3 directionSum = new Vector3(0.0f, 0.0f, 0.0f);
			if (offset >= 0)
			{
				int neighborIteratorAdd = useDiagonalDirections ? 1 : 2;
				
				distanceAtCoordinate = distanceField.DataRead[offset];
				for (int n = 0; n < 8; n += neighborIteratorAdd)
				{
					if (n < 4)
					{
						getDirectionTempDistanceArray[n] = 0.0f;
					}

					// If out of bounds then get away from the border
					float distanceDelta = -distanceToNeighbor[n];

					int neighborOffset = grid.GetNeighborOffset(offset, n);
					if (neighborOffset >= 0)
					{
						// Valid neighbor, not out of bounds
						bool blocked = obstacleField != null ? obstacleField.DataRead[neighborOffset] != 0 : false;
						float neighborDistance = distanceField.DataRead[neighborOffset];
						if (neighborDistance != sDistanceNotSet && !blocked)
						{
							// If distance is positive then get towards the cell, if negative get away
							distanceDelta = distanceAtCoordinate - neighborDistance;

							// Check for opposite sides not cancelling each other out
							if (n < 4)
							{
								// Store distance for directions between 0 and 3 (half of the directions)
								getDirectionTempDistanceArray[n] = distanceDelta;
							}
							else if (distanceDelta > 0.0f && getDirectionTempDistanceArray[n - 4] == distanceDelta)
							{
								// Make 10% larger to ensure we don't get stuck to a place where two opposite sides have the same (smaller) value than us.
								distanceDelta *= 0.1f;
							}
						}
					}
					
					// Cumulate direction(s)
					directionSum += direction[n] * distanceDelta;
				}

				// Normalize to get the final result, this can be zero if at cell with distance value of 0.
				directionSum.Normalize();
			}
			return directionSum;
		}

		/// <summary>
		/// Prepare for new update iteration
		/// </summary>
		public override void UpdateIterationStart()
		{
			// Initialize or clear visited and openlist
			if (visited == null)
			{
				visited = new bool[Grid.Instance.NumCells];
			}
			Array.Clear(visited, 0, visited.Length);

			WriteMaxDistance = 1.0f;

			List<GridSource> allSources = new List<GridSource>();

			// Add explicit sources
			if (explicitSources != null && explicitSources.Count > 0)
			{
				allSources.AddRange(explicitSources);
			}
			else
			{
				// Find compatible sources (layer is within layer mask)
				AddGridSources(allSources, sourceLayerMask);
			}

			GridField<int> obstacleGridField = obstacleProcessor != null ? obstacleProcessor.GridField : null;
			Grid grid = Grid.Instance;

			int neighborIteratorAdd = useDiagonalDirections ? 1 : 2;

			// Add start points
			for (int i = 0; i < allSources.Count; i++)
			{
				if (allSources[i] != null && allSources[i].enabled)
				{
					GridCoordinate coordinate = allSources[i].transform.position.ToGridCoordinate();
					if (coordinate.Valid)
					{
						int offset = coordinate.Offset;

						nodeQueue.Enqueue(offset, 0.0f);
						GridField.DataWrite[offset] = 0.0f;
						visited[offset] = true;

						// Iterate through cell neighbors
						for (int n = 0; n < 8; n += neighborIteratorAdd)
						{
							int neighborOffset = grid.GetNeighborOffset(offset, n);
							if (neighborOffset >= 0)
							{
								// Neighbor is valid
								bool neighborBlocked = obstacleGridField != null ? obstacleGridField.DataRead[neighborOffset] != 0 : false;

								if (!neighborBlocked && !visited[neighborOffset])
								{
									// Not blocked and not yet visited -> Add to the priority queue
									nodeQueue.Enqueue(neighborOffset, distanceToNeighbor[n]);
								}
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Process next iteration, if returns 'false' then further Updates are needed, 'true' is returned when processing is completed fully.
		/// </summary>
		public override bool UpdateIteration()
		{
			GridField<int> obstacleGridField = obstacleProcessor != null ? obstacleProcessor.GridField : null;
			Grid grid = Grid.Instance;

			int iterations = debugShowSlowMotionFill > 0 ? debugShowSlowMotionFill : cellIterationsPerFrame;

			int neighborIteratorAdd = useDiagonalDirections ? 1 : 2;

			// Iterate cells (breadth-first fill with distance sorted openlist)
			int count = 0;
			while ((iterations < 0 || count < iterations) && nodeQueue.Count > 0)
			{
				OpenListEntry current = nodeQueue.Dequeue();
				if (!visited[current.offset])
				{
					// Not visited yet -> Write distance and set visited
					visited[current.offset] = true;
					GridField.DataWrite[current.offset] = current.distance;

					// Update max distance (for debug color normalization)
					WriteMaxDistance = Mathf.Max(current.distance, WriteMaxDistance);

					// Iterate through cell neighbors
					for (int n = 0; n < 8; n += neighborIteratorAdd)
					{
						int neighborOffset = grid.GetNeighborOffset(current.offset, n);
						if (neighborOffset >= 0)
						{
							// Neighbor is valid
							bool neighborBlocked = obstacleGridField != null ? obstacleGridField.DataRead[neighborOffset] != 0: false;

							if (!neighborBlocked && !visited[neighborOffset])
							{
								// Not blocked and not yet visited -> Add to the priority queue
								nodeQueue.Enqueue(neighborOffset, current.distance + distanceToNeighbor[n]);
							}
						}
					}
				}
				
				count++;
			}
		
			// Check if we're done
			return nodeQueue.Count == 0;
		}

		/// <summary>
		/// Update iteration done
		/// </summary>
		public override void UpdateIterationEnd()
		{
			// We're done -> Flip buffer (read/write) so we can read the latest info
			GridField.Flip();
			ReadMaxDistance = WriteMaxDistance;
		}

		/// <summary>
		/// Update is on-going if we have openList and it has any elements in it.
		/// </summary>
		public override bool UpdateIterationInProgress
		{
			get
			{
				return nodeQueue != null && nodeQueue.Count > 0;
			}
		}

		#region Debug drawing

#if UNITY_EDITOR

		/// <summary>
		/// Get color for specified cell (green = closer to source, red = further from source)
		/// </summary>
		public override Color GetCellColor(int offset)
		{
			float[] buffer = debugShowSlowMotionFill > 0 ? GridField.DataWrite : GridField.DataRead;
			if (buffer[offset] == sDistanceNotSet)
			{
				return new Color(0.0f, 0.0f, 0.0f, 0.7f);
			}

			float distance = buffer[offset];

			if (distance >= sDistanceNotSet)
			{
				return new Color(0.0f, 0.0f, 0.0f, 0.7f);
			}

			float alpha = buffer[offset] / ReadMaxDistance;

			float r = Mathf.Lerp(0.0f, 1.0f, alpha);
			float g = Mathf.Lerp(1.0f, 0.0f, alpha);
			return new Color(r, g, 0.0f, 0.7f);
		}

		public override void DebugDrawCell(int offset)
		{
			base.DebugDrawCell(offset);
			if (debugDrawDirections)
			{
				Gizmos.color = new Color(1.0f, 1.0f, 1.0f, 0.7f);
				Vector3 cellCenter = Grid.Instance.GetPosition(offset);
				float distanceAtCoordinate = 0.0f;
				Vector3 direction = GetDirection(GridField, obstacleProcessor.GridField, offset, out distanceAtCoordinate);
				Gizmos.DrawLine(cellCenter, cellCenter + direction * Grid.Instance.CellSize * 0.4f);
			}
		}

		[SerializeField]
		private int debugShowSlowMotionFill = -1;

		[SerializeField]
		private bool debugDrawDirections = false;

#endif

		#endregion

		private static float[] getDirectionTempDistanceArray = new float[4];

		// Sorted open list and visited book-keeping
		private OpenList nodeQueue = new OpenList();
		private bool[] visited;

		// Static helpers for normalized cell distances
		private static float SQRT_2 = 1.414213562373095f;
		private static float[] distanceToNeighbor = new float[] { 1.0f, SQRT_2, 1.0f, SQRT_2, 1.0f, SQRT_2, 1.0f, SQRT_2 };

		private static Vector3[] direction = new Vector3[8] {	new Vector3(0.0f, 0.0f, -1.0f), new Vector3(1.0f, 0.0f, -1.0f), new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.0f, 1.0f), 
																new Vector3(0.0f, 0.0f, 1.0f), new Vector3(-1.0f, 0.0f, 1.0f), new Vector3(-1.0f, 0.0f, 0.0f), new Vector3(-1.0f, 0.0f, -1.0f), };

		// Statistics for debug drawing
		public float ReadMaxDistance { get; set; }
		public float WriteMaxDistance { get; set; }

		// Explicit sources for distance field
		[SerializeField, Tooltip("Explicit sources, source layer is ignored if it is in this list.")]
		private List<GridSource> explicitSources = new List<GridSource>();

		// Source layer mask for accepting specific grid sources
		[SerializeField, Tooltip("GridSource layer mask, which GridSource objects to include as sources for this distance field (1 << layer). Default -1 is all bits set (accepts everything).")]
		private int sourceLayerMask = -1;

		[SerializeField, Tooltip("Should use diagonal directions also (eight directions) or only four cardinal directions.")]
		private bool useDiagonalDirections = true;

		// Obstacle processor to access obstacle field
		[SerializeField, Tooltip("Obstacle processor reference to access the obstacle field.")]
		private ObstacleProcessor obstacleProcessor;

		// How many iterations / frame
		[SerializeField, Tooltip("How many cells to process per frame. Default is -1 which will process all cells at once.")]
		private int cellIterationsPerFrame = -1;
	}
}