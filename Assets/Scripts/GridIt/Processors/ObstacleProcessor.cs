using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridIt
{
	/// <summary>
	/// Obstacle info helper for the processing queue
	/// </summary>
	public class ObstacleInfo
	{
		public ObstacleInfo(Vector3 aabbMin, Vector3 aabbMax, Vector3 localSize, Matrix4x4 worldToLocal, float distanceThreshold, int deltaChange)
		{
			AABBMin = aabbMin;
			AABBMax = aabbMax;
			LocalSize = localSize;
			DeltaChange = deltaChange;
			WorldToLocal = worldToLocal;
			DistanceThreshold = distanceThreshold;
		}

		public Vector3 AABBMin;
		public Vector3 AABBMax;
		public Vector3 LocalSize;
		public int DeltaChange;
		public Matrix4x4 WorldToLocal;
		public float DistanceThreshold;
	}

	/// <summary>
	/// Create obstacle field where all BoxCollider OBBs are painted in the boolean field.
	/// </summary>
	public class ObstacleProcessor : GridProcessor
	{
		/// <summary>
		/// Create grid field and setup for "not processing" state.
		/// </summary>
		public override void InitProcessor()
		{
			GridField = new GridField<int>(Grid.Instance.Width, Grid.Instance.Height, 0, false);

			Grid grid = Grid.Instance;

			grid.GridObstacleAdded += OnGridObstacleAdded;
			grid.GridObstacleRemoved += OnGridObstacleRemoved;
			grid.GridObstacleTransformChanged += OnGridObstacleTransformChanged;

			foreach (GridObstacle gridObstacle in grid.GridObstacles)
			{
				obstaclesToProcess.Enqueue(new ObstacleInfo(gridObstacle.AABBMin, gridObstacle.AABBMax, gridObstacle.LocalSize, gridObstacle.WorldToLocal, distanceThreshold, 1));
			}
		}

		/// <summary>
		/// Reset obstacle field (re-build it)
		/// </summary>
		public void Reset()
		{
			Grid grid = Grid.Instance;
			if (GridField != null)
			{
				GridField.Clear();
				foreach (GridObstacle gridObstacle in grid.GridObstacles)
				{
					obstaclesToProcess.Enqueue(new ObstacleInfo(gridObstacle.AABBMin, gridObstacle.AABBMax, gridObstacle.LocalSize, gridObstacle.WorldToLocal, distanceThreshold, 1));
				}
			}
		}

		public override void UninitProcessor()
		{
			base.UninitProcessor();

			Grid grid = Grid.Instance;
			if (grid != null)
			{
				grid.GridObstacleAdded -= OnGridObstacleAdded;
				grid.GridObstacleRemoved -= OnGridObstacleRemoved;
				grid.GridObstacleTransformChanged -= OnGridObstacleTransformChanged;			
			}
		}

		private void OnGridObstacleAdded(GridObstacle gridObstacle)
		{
			obstaclesToProcess.Enqueue(new ObstacleInfo(gridObstacle.AABBMin, gridObstacle.AABBMax, gridObstacle.LocalSize, gridObstacle.WorldToLocal, distanceThreshold, 1));
		}

		private void OnGridObstacleRemoved(GridObstacle gridObstacle)
		{
			obstaclesToProcess.Enqueue(new ObstacleInfo(gridObstacle.AABBMin, gridObstacle.AABBMax, gridObstacle.LocalSize, gridObstacle.WorldToLocal, distanceThreshold, -1));
		}

		private void OnGridObstacleTransformChanged(GridObstacle gridObstacle)
		{
			obstaclesToProcess.Enqueue(new ObstacleInfo(gridObstacle.PreviousAABBMin, gridObstacle.PreviousAABBMax, gridObstacle.PreviousLocalSize, gridObstacle.PreviousWorldToLocal, distanceThreshold, -1));
			obstaclesToProcess.Enqueue(new ObstacleInfo(gridObstacle.AABBMin, gridObstacle.AABBMax, gridObstacle.LocalSize, gridObstacle.WorldToLocal, distanceThreshold, 1));
		}

		/// <summary>
		/// Process one obstacle and add deltaChange to the grid cells it affects.
		/// This function will use the OBB of the obstacle and not simply the AABB which would yield incorrect results.
		/// It uses two separate thresholds for Y axis and X, Z axes (2d plane).
		/// </summary>
		private void ProcessObstacle(ObstacleInfo obstacleInfo)
		{
			GridCoordinate upperLeft = obstacleInfo.AABBMin.ToGridCoordinate();
			GridCoordinate lowerRight = obstacleInfo.AABBMax.ToGridCoordinate();
			upperLeft.Clamp();
			lowerRight.Clamp();
			Matrix4x4 worldToLocal = obstacleInfo.WorldToLocal;

			float sqrDistanceThreshold = obstacleInfo.DistanceThreshold * obstacleInfo.DistanceThreshold;
			if (upperLeft.Valid && lowerRight.Valid)
			{
				for (int y = upperLeft.y; y <= lowerRight.y; y++)
				{
					for (int x = upperLeft.x; x <= lowerRight.x; x++)
					{
						GridCoordinate current = new GridCoordinate(x, y);

						// Get cell center in local coordinates
						Vector3 localPosition = worldToLocal.MultiplyPoint(current.ToVector3());

						// Calculate distance to OBB in the local X axis (xDiff)
						float xDiffL = -localPosition.x - obstacleInfo.LocalSize.x * 0.5f;
						float xDiffR = localPosition.x - obstacleInfo.LocalSize.x * 0.5f;
						float xDiff = Mathf.Max(0.0f, Mathf.Max(xDiffL, xDiffR));

						// Calculate distance to OBB in the local Y axis (yDiff)
						float yDiffL = -localPosition.y - obstacleInfo.LocalSize.y * 0.5f;
						float yDiffR = localPosition.y - obstacleInfo.LocalSize.y * 0.5f;
						float yDiff = Mathf.Max(0.0f, Mathf.Max(yDiffL, yDiffR));

						// Calculate distance to OBB in the local Z axis (zDiff)
						float zDiffL = -localPosition.z - obstacleInfo.LocalSize.z * 0.5f;
						float zDiffR = localPosition.z - obstacleInfo.LocalSize.z * 0.5f;
						float zDiff = Mathf.Max(0.0f, Mathf.Max(zDiffL, zDiffR));

						float diff = xDiff * xDiff + zDiff * zDiff;
						if (diff <= sqrDistanceThreshold && (collisionHeightThreshold < 0.0f || yDiff <= collisionHeightThreshold))
						{
							GridField.DataWrite[current.Offset] += obstacleInfo.DeltaChange;
						}
					}
				}
			}
		}

		/// <summary>
		/// Process next iteration, if returns 'false' then further Updates are needed, 'true' is returned when processing is completed fully.
		/// </summary>
		public override bool UpdateIteration()
		{
			while (obstaclesToProcess.Count > 0)
			{
				ObstacleInfo obstacleInfo = obstaclesToProcess.Dequeue();
				ProcessObstacle(obstacleInfo);
			}

			// Everything is processed
			return true;
		}

		/// <summary>
		/// Is processing still on-going, if we have obstacles to process.
		/// </summary>
		public override bool UpdateIterationInProgress
		{
			get
			{
				return obstaclesToProcess != null && obstaclesToProcess.Count > 0;
			}
		}

		/// <summary>
		/// Data GridField where results are written, the number means how many overlapping grid obstacles affect given cell.
		/// </summary>
		public GridField<int> GridField
		{
			get;
			private set;
		}

		#region Debug drawing

#if UNITY_EDITOR

		private Color[] debugColors = new Color[7] { Color.cyan, Color.magenta, Color.yellow, Color.gray, Color.red, Color.green, Color.blue };

		/// <summary>
		/// Get color for specified cell
		/// </summary>
		public override Color GetCellColor(int offset)
		{
			int count = GridField.DataRead[offset];
			int debugColor = (count - 1) % debugColors.Length;
			return count > 0 ? debugColors[debugColor] : new Color(0.0f, 0.0f, 0.0f, 0.7f);
		}

		private void OnValidate()
		{
			Reset();
		}
#endif

		#endregion

		// Update setup
		private Queue<ObstacleInfo> obstaclesToProcess = new Queue<ObstacleInfo>();

		// Distance threshold for Y axis
		[SerializeField, Tooltip("Specific distance threshold for height, how far from obstacle OBB can the grid cell center be to be blocked along the Y axis.")]
		private float collisionHeightThreshold = 3.0f;

		// Distance threshold for X, Z axes
		[SerializeField, Tooltip("How far from obstacle OBB can the grid cell center be to be blocked. Default is 0 meaning the cell center must be contained within OBB to be blocked.")]
		private float distanceThreshold = 0.0f;
	}
}
