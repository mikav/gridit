using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridIt
{
	/// <summary>
	/// This processor is made to act as a search structure for finding nearby grid sources to a position.
	/// It will store list of indices per each cell which refer to grid source list in the Grid singleton.
	/// This has a maximum number of entries per cell which is exposed. It won't break if there are more than the maximum number of sources affecting a cell while it would simply ignore the excess ones.
	/// If the maximum number is too low, and there are a lot of excess (ignored) sources, then any calculation such as avoidance will give incorrect results and may lead to jittering or oscillation.
	/// If the maximum number is too high this processor will consume a lot of memory and potentially cause performance issues due to accessing large amounts of memory at random.
	/// Indices are 32bit integers so having 64 x 64 cells in a grid with maximum of 64 grid sources influencing any given cell it would consume 64 * 64 * 64 * 4 = 1048576 bytes of memory.
	/// </summary>
	public class OccupancyProcessor : GridProcessor
	{
		/// <summary>
		/// Create grid field and setup for "not processing" state.
		/// </summary>
		public override void InitProcessor()
		{
			GridField = new GridField<short>(Grid.Instance.Width, Grid.Instance.Height * maxNumberOfActorsPerCell, 0);
			CountGridField = new GridField<byte>(Grid.Instance.Width, Grid.Instance.Height, 0);
		}

		/// <summary>
		/// Prepare for new update iteration.
		/// </summary>
		public override void UpdateIterationStart()
		{
			WriteMaxCount = int.MinValue;
		}

		/// <summary>
		/// Process next iteration, if returns 'false' then further Updates are needed, 'true' is returned when processing is completed fully.
		/// </summary>
		public override bool UpdateIteration()
		{
			Grid grid = Grid.Instance;
			int currentCount = 0;
			List<GridSource> sourcesToProcess = Grid.Instance.GridSources;
			for (int i = 0; i < sourcesToProcess.Count; i++)
			{
				if (sourcesToProcess[i].HasCompatibleLayer(sourceLayerMask))
				{
					int offset = grid.GetOffset(sourcesToProcess[i].transform.position);
					if (offset >= 0)
					{
						for (int n = 0; n < 8; n++)
						{
							int neighborOffset = grid.GetNeighborOffset(offset, n);
							if (neighborOffset >= 0)
							{
								currentCount = CountGridField.DataWrite[neighborOffset];
								CountGridField.DataWrite[neighborOffset]++;

								GridField.DataWrite[neighborOffset * maxNumberOfActorsPerCell + currentCount] = (short)i;

								// Update max distance (for debug color normalization)
								WriteMaxCount = Mathf.Max(currentCount + 1, WriteMaxCount);
							}
						}

						currentCount = CountGridField.DataWrite[offset];
						CountGridField.DataWrite[offset]++;

						GridField.DataWrite[offset * maxNumberOfActorsPerCell + currentCount] = (short)i;

						// Update max distance (for debug color normalization)
						WriteMaxCount = Mathf.Max(currentCount + 1, WriteMaxCount);
					}
				}
			}

			// Need to continue calculations
			return true;
		}

		/// <summary>
		/// Update iteration done.
		/// </summary>
		public override void UpdateIterationEnd()
		{
			// Calculations done -> Flip buffers so we can read results (do not need to clear the write buffer on the GridField)
			GridField.Flip(false);
			CountGridField.Flip();
			ReadMaxCount = WriteMaxCount;
		}

		/// <summary>
		/// Is processing still on-going, if grid sources are still to be processed.
		/// </summary>
		public override bool UpdateIterationInProgress
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Data GridField where results are written.
		/// Note! This grid field will be Grid.Instance.NumCells * MaxNumberOfActorsPerCell in size.
		/// Use CountGridField to check how many entries are 'active' at each cell.
		/// </summary>
		public GridField<short> GridField
		{
			get;
			private set;
		}

		/// <summary>
		/// Data GridField for active count in each of the GridField strides.
		/// </summary>
		public GridField<byte> CountGridField
		{
			get;
			private set;
		}

		#region Debug drawing

#if UNITY_EDITOR

		/// <summary>
		/// Get color for specified cell
		/// </summary>
		public override Color GetCellColor(int offset)
		{
			int count = CountGridField.DataRead[offset];

			if (count == 0)
			{
				return new Color(0.0f, 0.0f, 0.0f, 0.7f);
			}

			float alpha = (float)count / (float)ReadMaxCount;

			return new Color(alpha, 0.0f, 0.0f, 0.7f);
		}

		// Statistics for debug drawing
		public int ReadMaxCount
		{
			get;
			set;
		}

		public int WriteMaxCount
		{
			get;
			set;
		}

#endif

		#endregion

		/// <summary>
		/// How many actors per cell can be stored.
		/// </summary>
		public int MaxNumberOfActorsPerCell
		{
			get
			{
				return maxNumberOfActorsPerCell;
			}
		}

		// Source layer mask for accepting specific grid sources
		[SerializeField, Tooltip("GridSource layer mask, which GridSource objects to include as sources for this distance field (1 << layer).")]
		private int sourceLayerMask = -1;

		// How many actors per cell can be stored
		[SerializeField, Tooltip("Max number of actors per cell that can be stored. If there are more actors in the influence area of a cell they will be ignored.")]
		private int maxNumberOfActorsPerCell = 64;
	}
}
